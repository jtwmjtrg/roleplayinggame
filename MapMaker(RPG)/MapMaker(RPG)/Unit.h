#pragma once
#include "DxLib.h"
#include "Font.h"
#include "Input.h"
#include <vector>
#include <string>
#include <boost/lexical_cast.hpp>
#include "Panel.h"

using namespace std;

// ユニットデータのスーパークラス
class UnitData {
protected:
	//【画像】
	static int gr_Mode;
	static int gr_Move;
	static int gr_If;
	static int gr_Act;
	// 編集画面の背景
	// 挙動
	static int gr_Editing_Act_Warp;
	static int gr_Editing_Act_Talk1;
	static int gr_Editing_Act_Talk2;
	static int gr_Editing_Act_Battle;
	static int gr_Editing_Act_Damage1;
	static int gr_Editing_Act_Damage_Target; 
	static int gr_Editing_Act_Recovery;
	static int gr_Editing_Act_Item;
	static int gr_Editing_Act_Shop;
	static int gr_Editing_Act_Existence;
	static int gr_Editing_Act_ChangeMode;
	static int gr_Editing_Act_Message;
	static int gr_Editing_Act_SelectBranch1;
	static int gr_Editing_Act_SelectBranch2;
	static int gr_Editing_Act_End;
	static int gr_Editing_Act_Goto;
	static int gr_Editing_Act_GameOver;
	static int gr_Editing_Act_GameClear;
	// 挙動の実行条件
	static int gr_Editing_If_Talk;
	static int gr_Editing_If_Touch;
	static int gr_Editing_If_Message;
	static int gr_Editing_If_Always;
	static int gr_Editing_If_Distance;
	// 移動
	static int gr_Editing_Move_None;
	static int gr_Editing_Move_Random;
	// モード
	static int gr_Editing_Mode_None;
	static int gr_Editing_Mode_Chapter;
	static int gr_Editing_Mode_Chapter2;
	// 種類変更時のリスト
	static int gr_ModeList;		// モードリスト
	static int gr_MoveList;		// 移動リスト
	static int gr_IfList;		// 挙動の実行条件リスト
	static int gr_ActList;		// 挙動リスト

	static int gr_Add_Des;		// 追加,削除ボタン

	//【変数】
	bool ChangeTypeFlag;	// 種類の変更フラグ
	int scroll;				// リストのスクロール

	int dataType;		// データの種類
	int detailsType;	// 詳細区分
	int change_detailsType;	// 区分変更用

	// パネルクラス
	std::vector<Panel*> panel;
	virtual void SetPanel() {}

public:
	UnitData();
	virtual ~UnitData();

	static void Load();		// 画像読み込み
	static void Release();	// 解放処理

	virtual bool Editing();	// データ編集

	virtual void DrawSimple(int y) = 0;		// 簡易表示（右側のやつ）
	virtual void DrawData();			// 詳細表示

	virtual std::string GetCsv() = 0;		// csvデータに変換してデータを取得
};

// 【非対応データ】
class UnitData_Default : public UnitData {
	std::string line;
public:
	UnitData_Default(std::string line);
	~UnitData_Default();
	bool Editing();
	void DrawSimple(int y);
	void DrawData();
	std::string GetCsv();
};

/*-------------------------------------------------ユニットの挙動データ--------------------------------------------*/

enum class eMUAReturn : __int8 {
	Next = 0,	// すぐに次の挙動にうつる
	Stop = 1,	// 一時停止
	End = -1,	// 強制終了
};

// ユニットの挙動
class MapUnit_Action : public UnitData {	// 抽象クラス
protected:
	// アクセス制限されたコンストラクタ
	MapUnit_Action();
	
public:
	// デストラクタ
	virtual ~MapUnit_Action() {}
	
	virtual void DrawSimple(int y);		// 簡易表示（右側のやつ）
};

// 【転移】
class MapUnit_Action_Warp : public MapUnit_Action {
private:
	// ステータス
	int mapId;	// 転移先のマップID
	int x;		// 転移先の座標
	int y;
	int effect;	// 転移エフェクトの種類

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Warp();
	MapUnit_Action_Warp(std::string line);
	~MapUnit_Action_Warp();
	
	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

// 【セリフ】
class MapUnit_Action_Talk : public MapUnit_Action {
private:
	// ステータス
	std::vector<std::string> str;	// セリフ内容
	
	std::vector<Panel*> serifPanel;	// セリフのパネル

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Talk();
	MapUnit_Action_Talk(std::string line);
	~MapUnit_Action_Talk();

	bool Editing();
	void DrawData();
	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

// 【戦闘】
// 戦闘終了の状態
enum class eEndStatus : __int8 {
	Win,	// 敵を全滅させた
	Lose,	// プレイヤーが全滅した
	Escape,	// 逃走成功
};
class MapUnit_Action_Battle : public MapUnit_Action {
private:
	int encID;			// エンカウントID
	int backID;			// 背景ID
	std::string bgmPath;// BGMのパス
	bool escapeFlag;	// 逃走可能フラグ
	bool loseEventFlag;	// 負けイベフラグ
	std::array<int, 3> branch;	// 戦闘終了の状態で分岐（挙動番号のジャンプ数）

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Battle();
	MapUnit_Action_Battle(std::string line);
	~MapUnit_Action_Battle();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

// 【ダメージ】
class MapUnit_Action_Damage : public MapUnit_Action {
private:
	int damage;	// ダメージ量（固定ダメージ）
	int target;	// 対象	0:全員	1:先頭	2:ランダム
	
public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Damage();
	MapUnit_Action_Damage(std::string line);
	~MapUnit_Action_Damage();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

// 【回復】
class MapUnit_Action_Recovery : public MapUnit_Action {
private:
	int heal_HP;		// HP回復量（固定量）	-1:全快
	int heal_MP;		// MP回復量（固定量）	-1:全快
	bool resurrection;	// 蘇生フラグ
	int target;			// 対象	0:全員	1:先頭	2:ランダム

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Recovery();
	MapUnit_Action_Recovery(std::string line);
	~MapUnit_Action_Recovery();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

// 【アイテム取得】
class MapUnit_Action_Item : public MapUnit_Action {
private:
	int itemID;		// 取得アイテムのID
	int jump_True;	// アイテムを手に入れた場合のジャンプ
	int jump_False;	// 持ち物がいっぱいだった場合のジャンプ

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Item();
	MapUnit_Action_Item(std::string line);
	~MapUnit_Action_Item();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

//【店】
class MapUnit_Action_Shop : public MapUnit_Action {
private:
	std::string shopID;		// 店ID

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Shop();
	MapUnit_Action_Shop(std::string line);
	~MapUnit_Action_Shop();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

//【消滅-出現】
class MapUnit_Action_Existence : public MapUnit_Action {
private:
	bool flag;		// true::出現	false:消滅
	int staging;	// 演出

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Existence();
	MapUnit_Action_Existence(std::string line);
	~MapUnit_Action_Existence();

	void DrawSimple(int y);

	std::string GetCsv();
};


//【モード変更】
class MapUnit_Action_ChangeMode : public MapUnit_Action {
private:
	int modeNum;	// 変更後のモード番号

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_ChangeMode();
	MapUnit_Action_ChangeMode(std::string line);
	~MapUnit_Action_ChangeMode();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

//【メッセージ発信】
class MapUnit_Action_Message : public MapUnit_Action {
private:
	int messageNum;	// メッセージ番号

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Message();
	MapUnit_Action_Message(std::string line);
	~MapUnit_Action_Message();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

//【選択分岐】
// 選択分岐データ
struct tSelectBranch {
	std::string answer;		// 選択肢文
	int destination;		// 分岐先（挙動番号をn進める）
};
enum class eSelectBranch : __int8 {
	question,	// 質問
	wait,		// 解答待ち
};
class MapUnit_Action_SelectBranch : public MapUnit_Action {
private:
	std::string question;	// 質問文
	std::vector<tSelectBranch> choices;	// 選択肢

	std::vector<Panel*> choicesPanel;	// 選択肢のパネル
public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_SelectBranch();
	MapUnit_Action_SelectBranch(std::string line);
	~MapUnit_Action_SelectBranch();

	bool Editing();
	void DrawData();
	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

//【強制終了】
class MapUnit_Action_End : public MapUnit_Action {
public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_End();
	~MapUnit_Action_End();

	void DrawSimple(int y);

	std::string GetCsv();
};

//【挙動番号の移動】
class MapUnit_Action_Goto : public MapUnit_Action {
private:
	int moveNum;	// 移動数

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_Goto();
	MapUnit_Action_Goto(std::string line);
	~MapUnit_Action_Goto();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

//【強制ゲームオーバー】
class MapUnit_Action_GameOver : public MapUnit_Action {
public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_GameOver();
	~MapUnit_Action_GameOver();

	void DrawSimple(int y);

	std::string GetCsv();
};

//【ゲームクリア】
class MapUnit_Action_GameClear : public MapUnit_Action {
public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_GameClear();
	~MapUnit_Action_GameClear();

	void DrawSimple(int y);

	std::string GetCsv();
};

// 【金を払う】
class MapUnit_Action_PayMoney : public MapUnit_Action {
private:
	int price;		// 払う金額
	int jump_True;	// 金が足りた場合のジャンプ
	int jump_False;	// 金が足りなかった場合のジャンプ
public:
	// コンストラクタ＆デストラクタ
	MapUnit_Action_PayMoney();
	MapUnit_Action_PayMoney(string line);
	~MapUnit_Action_PayMoney();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

/*-------------------------------------------------ユニットの挙動条件データ--------------------------------------------*/

// ユニットの挙動の条件
class MapUnit_If : public UnitData {	// 抽象クラス
protected:
	// アクセス制限されたコンストラクタ
	MapUnit_If();

public:
	// デストラクタ
	virtual ~MapUnit_If() {}

	virtual void DrawSimple(int y);		// 簡易表示（右側のやつ）
};

//【主人公がこのユニットに話しかける】
class MapUnit_If_Talk : public MapUnit_If {
public:
	// コンストラクタ＆デストラクタ
	MapUnit_If_Talk();
	~MapUnit_If_Talk();

	void DrawSimple(int y);

	std::string GetCsv();
};

//【主人公がこのユニットに触れる】
class MapUnit_If_Touch : public MapUnit_If {
public:
	// コンストラクタ＆デストラクタ
	MapUnit_If_Touch();
	~MapUnit_If_Touch();
	
	void DrawSimple(int y);
	std::string GetCsv();
};

//【メッセージ受信】
class MapUnit_If_Message : public MapUnit_If {
private:
	int messageNum;	// メッセージ番号
public:
	// コンストラクタ＆デストラクタ
	MapUnit_If_Message();
	MapUnit_If_Message(std::string line);
	~MapUnit_If_Message();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

//【常時】
class MapUnit_If_Always : public MapUnit_If {
public:
	// コンストラクタ＆デストラクタ
	MapUnit_If_Always();
	~MapUnit_If_Always();

	void DrawSimple(int y);
	std::string GetCsv();
};

//【プレイヤーとの距離判定】
class MapUnit_If_Distance : public MapUnit_If {
private:
	int distance;	// 距離（半径）
	bool In_Out;	// true:近づく	false:離れる
public:
	// コンストラクタ＆デストラクタ
	MapUnit_If_Distance();
	MapUnit_If_Distance(std::string line);
	~MapUnit_If_Distance();

	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

/*-------------------------------------------------ユニットの移動データ--------------------------------------------*/

// ユニットの移動
class MapUnit_Move : public UnitData {	// 抽象クラス
protected:
	int speed;		// 移動速度
	int delay;		// １マス移動するごとにnF止まる

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Move();
	virtual ~MapUnit_Move() {}
	
	virtual void DrawSimple(int y);		// 簡易表示（右側のやつ）
};

// 【動かない】
class MapUnit_Move_None : public MapUnit_Move {
public:
	// コンストラクタ＆デストラクタ
	MapUnit_Move_None();
	~MapUnit_Move_None();
	
	void DrawSimple(int y);
	std::string GetCsv();
};

// 【プレイヤーを追う or 逃げる】
class MapUnit_Move_chase : public MapUnit_Move {
private:
	bool chaseFlag;	// true:追う false:逃げる

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Move_chase();
	MapUnit_Move_chase(string line);
	~MapUnit_Move_chase();

	void DrawSimple(int y);
	void SetPanel();
	std::string GetCsv();
};

// 【ループ移動】
struct tUnitLoopMove {
	int dir;		// 移動方向
	int moveNum;	// 移動数
};
class MapUnit_Move_Loop : public MapUnit_Move {
private:
	int moveCount;	// 移動数
	int stepCount;	// 移動ステップ数

	std::vector<tUnitLoopMove> moveData;
	std::vector<Panel*> movePanel;	// 移動データのパネル

public:
	// コンストラクタ＆デストラクタ
	MapUnit_Move_Loop();
	MapUnit_Move_Loop(std::string line);
	~MapUnit_Move_Loop();

	bool Editing();
	void DrawData();
	void DrawSimple(int y);
	void SetPanel();
	std::string GetCsv();
};

//【ランダム移動】
class MapUnit_Move_Random : public MapUnit_Move {
public:
	// コンストラクタ＆デストラクタ
	MapUnit_Move_Random();
	MapUnit_Move_Random(std::string line);
	~MapUnit_Move_Random();
	
	void DrawSimple(int y);
	void SetPanel();

	std::string GetCsv();
};

/*-------------------------------------------------ユニットのモード--------------------------------------------*/

//【モードの移行条件スーパークラス】
class MapUnit_Mode : public UnitData {
protected:
	int modeNum;		// 実行中の条件の要素番号
	bool collisionFlag;	// 当たり判定
	bool appearFlag;	// 出現フラグ

	MapUnit_Mode();
	virtual ~MapUnit_Mode() {}
public:

};

//【条件なし】
class MapUnit_Mode_None : public MapUnit_Mode {
public:
	MapUnit_Mode_None();
	MapUnit_Mode_None(std::string line);
	~MapUnit_Mode_None();

	void DrawSimple(int y);

	std::string GetCsv();
};

//【ストーリー進行度】
class MapUnit_Mode_Chapter : public MapUnit_Mode {
private:
	int chapter;	// 基準の進行度
	int term;		// 条件（0:のとき	1:以外のとき	2:以前		3:以降）

	// 編集画面用
	bool termSelectFlag;	// 対象選択フラグ

public:
	MapUnit_Mode_Chapter();
	MapUnit_Mode_Chapter(std::string line);
	~MapUnit_Mode_Chapter();

	bool Editing(); 	// データ編集
	void DrawSimple(int y);
	void DrawData();

	std::string GetCsv();
};

/*---------------------------------------------ユニット詳細データの追加画面--------------------------------------------*/

// ユニットデータのファクトリ
UnitData* Factory_UnitData(std::string line);
UnitData* Factory_UnitData(int dataType, int detailsType);

// データの種類
enum class eUnitData {
	Mode,	// モード
	Move,	// 移動
	If,		// 挙動の実行条件
	Act,	// 挙動
};
// ユニット詳細データの追加画面
class AddUnitData {
private:
	static int gr_back;			// 背景,枠
	static int gr_UnitDataList;	// データの種類リスト
	static int gr_ModeList;		// モードリスト
	static int gr_MoveList;		// 移動リスト
	static int gr_IfList;		// 挙動の実行条件リスト
	static int gr_ActList;		// 挙動リスト

	eUnitData unitDataType;				// データの種類
	int selectTypeList;					// 種類ごとのタイプの選択番号
	std::vector<UnitData*>& unitData;	// ユニット詳細データ（参照値）
	int insertPos;						// データの追加箇所

	bool selectFlag_UnitData;	// データの種類リストの選択フラグ
	bool selectFlag_TypeList;	// 種類ごとのタイプの選択フラグ

	int scroll;		// リストのスクロール
public:
	AddUnitData(std::vector<UnitData*>& unitData, int insertPos);
	~AddUnitData();

	static void Load();		// 画像読み込み
	static void Release();	// 解放処理
	
	bool UpDate();
	void Draw();
};