#pragma once
#include <fstream>
#include <iostream>
#include <sstream>
#include <map>
#include <boost/lexical_cast.hpp>
#include <deque>
#include "Scene.h"
#include "Unit.h"
#include "Panel.h"

#define CHIPSIZE 64;

using namespace std;

// マップチップデータ、ユニットデータに分割
enum class eMakeType : __int8 {
	Map,
	PutUnit,
};

// チップの種類
enum class eChipType : __int8 {
	FLO,	// 床
	WAL		// 壁
};
// 選択中のチップのデータ
struct tSelectChip {
	int data;// マップチップの内容
	eChipType type;	// チップの種類
	int x, y;	// 座標
};
// ユニットの種類
enum class eUnitType : __int8 {
	LocalWarp,	// ローカル転移
	GlobalWarp,	// グローバル転移
	NPC,	// 普通のNPC
	Shop,	// 店
	Enemy,	// シンボルエネミー
	Object	// オブジェクト
};
// ユニットごとの配置データ
struct tPutUnit{
	int id;				// ID
	std::string name;	// 名前
	int x, y;			// 座標
	int gr;				// 画像ID
	bool	collisionFlag;	// 当たり判定
	bool	appearFlag;		// 出現フラグ
	int mode;

	std::vector<UnitData*> unitData;	// ユニット詳細データ
};
// ユニット詳細データの削除時の確認画面
struct tDeleteUnitDataWindow {
	int gr_Back;		// 画像
	bool flag;			// 確認画面の表示フラグ
	int unitDataNum;	// 削除するデータの要素番号
};
// ユニット詳細データの種類変更時の確認画面
struct tChangeUnitDataWindow {
	int gr_Back;			// 画像
	bool flag;				// 確認画面の表示フラグ
	eUnitData unitDataType;	// データの種類
	int selectTypeList;		// 種類ごとのタイプの選択番号
};

class MapMaker : public SuperScene {
private:

	/*-----------------------------共通----------------------------------*/
	eMakeType mode;	// モード選択

	// 【画像】
	std::array<int, 2> gr_mode;	// 左上のモード選択の画像
	int gr_Back;	// 背景
	int gr_Cursor;	// カーソル
	int gr_SelectChip;	// 選択中のチップ
	int gr_AddLine;		// 行追加/削除の画像

	// スクロール
	int scrollX;
	int scrollY;

	// 選択中のマップ座標
	unsigned int selectX;
	unsigned int selectY;

	// チップサイズ
	int chipSize;	// チップのサイズ

	// エンカウント情報
	bool		encFlag;	// エンカウントフラグ
	vector<int>	encList;	// エンカウントリスト
	int encMinCount;	// 最小エンカウント歩数
	int encMaxCount;	// 最大エンカウント歩数

	// 【ファイル読み込み用】
	std::string dataPath;	// データのパス
	std::string L_mapPath;	// どのマップを読み込むか
	//【ファイル書き込み用】
	int SaveSuccess;	// セーブができたか

	/*-----------------------------マップ作製----------------------------------*/
	// 【マップチップの画像】
	std::map<int, int> gr_mapChip;	// マップチップの画像
	std::vector<int> gr_mcGrIDList_FLO;	// マップチップのリスト（床）
	std::vector<int> gr_mcGrIDList_WAL;	// マップチップのリスト（壁）
	
	// マップデータ
	std::deque<std::deque<int>> mapData;

	// 【チップリスト】
	std::array<int, 2> gr_ChipList;// 背景
	eChipType select_ChipList;
	// チップリストのスクロール
	int scroll_list_FLO;	// 床
	int scroll_list_WAL;	// 壁

	// 選択中チップ
	tSelectChip selectChip;

	// 削除したマップチップデータ（Ctrl+Zのやつ）
	deque<tSelectChip> preChipData;
	// 削除したマップチップを戻す用の配置データ（Ctrl+Yのやつ）
	std::vector<tSelectChip> postChipData;

	/*-----------------------------ユニット配置----------------------------------*/

	// 【画像】
	std::map<int,int[16]> gr_unit;	// ユニットの画像
	int gr_PutUnitStatus;		// ユニット配置データの枠
	int gr_UnitGrBack;			// 画像リストの枠

	std::vector<tPutUnit> putUnit;	// ユニット配置データ

	// 【ユニットリスト】
	std::array<int, 2> gr_UnitList;		// 背景
	std::vector<int> unitGrIDList;	// ユニットの画像のIDリスト

	// 選択中ユニット
	tPutUnit* selectUnit;
	bool unitMoveFlag;		// ユニットの移動フラグ
	bool unitGrSelectFlag;	// ユニットの画像選択フラグ
	bool unitIdChangeFlag;	// ユニットIDの変更フラグ
	
	// 削除したユニット配置データ（Ctrl+Zのやつ）
	std::vector<tPutUnit> prePutUnit;

	/*-----------------------------ユニットデータ----------------------------------*/
	bool unitDataSelectFlag;	// ユニット詳細データの選択フラグ
	int selectUnitDataNum;		// 選択したユニットデータの番号
	int scroll_UnitData;		// ユニットデータのリストのスクロール

	bool unitDataMoveFlag;		// ユニットの移動フラグ	
	tDeleteUnitDataWindow deleteUnitDataWindow;	// ユニット詳細データの削除時の確認画面
	AddUnitData* addUnitData;	// ユニット詳細データの追加画面
	
public:

	static InputData* inputData;	// 標準入力データ

	static tChangeUnitDataWindow changeUnitDataWindow;	// ユニット詳細データの種類変更時の確認画面

	MapMaker(std::string dataPath);
	~MapMaker();

	void LoadMapChip();	// マップチップ画像読み込み
	void LoadUnitGr();	// ユニット画像読み込み

	void UpDate();
	void UpDate_Map();
	void UpDate_Unit();
	void Draw();
	void Draw_Map();
	void Draw_Unit();

	void SetSelectChip(int data);

	void PutChip();	// 選択中のチップを置く

	void LoadMapData();	// マップデータ読み込み
	void SaveMapData();	// マップデータ書き込み
	
	void LoadPutUnit();	// ユニット配置読み込み
	void SavePutUnit();	// ユニット配置書き込み

	void LoadUnitData(tPutUnit* unit);	// ユニットごとのデータ読み込み
	void SaveUnitData(tPutUnit* unit);	// ユニットデータのセーブ

	void SaveUnitInitMode();	// ユニットの初期モードのセーブ
};