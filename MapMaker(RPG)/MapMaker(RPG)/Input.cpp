#include "Input.h"

//////////////////////////////////////////////キーボード////////////////////////////////////////////////////
KeyData::KeyData() {

}
KeyData::~KeyData() {

}
int KeyData::key[256];
char KeyData::tmpKey[256];
void KeyData::UpDate() {
	KeyData::tmpKey[256];			// 現在のキーの入力状態を格納する
	GetHitKeyStateAll(KeyData::tmpKey);	// 全てのキーの入力状態を得る
	for (int i = 0; i < 256; i++) {
		if (KeyData::tmpKey[i] != 0) {	// i番のキーコードに対応するキーが押されていたら
			KeyData::key[i]++;   // 加算
		}
		else if (KeyData::key[i] > 0) {	// キーが離された瞬間
			KeyData::key[i] = -1; // -1にする
		}
		else {	// それ以外
			KeyData::key[i] = 0; // 0にする
		}
	}
}
bool KeyData::CheckEnd() {
	// 強制終了
	// ESC + Ctrl
	// Ctrlは左右どちらでも可
	return KeyData::key[KEY_INPUT_ESCAPE] > 0 && (KeyData::key[KEY_INPUT_LCONTROL] > 0 || KeyData::key[KEY_INPUT_RCONTROL] > 0);
}
int KeyData::Get(int KeyCode) {
	return KeyData::key[KeyCode];
}

//////////////////////////////////////////////ゲームパッド////////////////////////////////////////////////////
PadData::PadData() {

}
PadData::~PadData() {

}
int PadData::button[4][16];  // ゲームパッドの入力状態格納用変数
XINPUT_STATE PadData::input[4];	// ゲームパッドのナンバー
__int8 PadData::padNum;
void PadData::SetPadNum() {
	PadData::padNum = (__int8)GetJoypadNum();
}
void PadData::UpDate(){
	for (int j = 0; j < PadData::padNum; j++) {
		// 入力状態を取得
		GetJoypadXInputState(DX_INPUT_PAD1, &PadData::input[j]);
		for (int i = 0; i < 16; i++) {
			if (PadData::input[j].Buttons[i] != 0) {	// i番のキーコードに対応するキーが押されていたら
				PadData::button[j][i]++;   // 加算
			}
			else if (button[i] > 0) {	// キーが離された瞬間
				PadData::button[j][i] = -1; // -1にする
			}
			else {				// それ以外
				PadData::button[j][i] = 0; // 0にする
			}
		}
	}
}
int PadData::Get(int code, int padNum) {	// ゲームパッドの入力状態取得
	return PadData::button[padNum][code];
}
bool PadData::CheckEnd() {
	return{
		(PadData::button[0][XINPUT_BUTTON_START] < 1 && PadData::button[0][XINPUT_BUTTON_BACK] < 1) ||
		(PadData::button[1][XINPUT_BUTTON_START] < 1 && PadData::button[1][XINPUT_BUTTON_BACK] < 1) ||
		(PadData::button[2][XINPUT_BUTTON_START] < 1 && PadData::button[2][XINPUT_BUTTON_BACK] < 1) ||
		(PadData::button[3][XINPUT_BUTTON_START] < 1 && PadData::button[3][XINPUT_BUTTON_BACK] < 1)
	};
}
//////////////////////////////////////////////マウス////////////////////////////////////////////////////
MouseData::MouseData(){

}
MouseData::~MouseData() {

}
int MouseData::x = 0;
int MouseData::y = 0;
int MouseData::pre_x = 0;
int MouseData::pre_y = 0;
int MouseData::wheel = 0;
int MouseData::mouse[3];
int MouseData::mouseInput;
void MouseData::UpDate() {
	// 座標
	pre_x = x;
	pre_y = y;
	GetMousePoint(&x, &y);
	// シフト押しながら->直線移動
	/*if (KeyData::Get(KEY_INPUT_LSHIFT) > 0) {
		if (x > y) {
			y = pre_y;
		}
		else {
			x = pre_x;
		}
	}*/

	// ホイール
	wheel = GetMouseWheelRotVol();
	//マウスの押した状態取得
	int MouseInput = GetMouseInput();
	for (int i = 0; i < 3; i++) {
		if ((MouseInput & 1 << i) != 0) mouse[i]++;   //押されていたらカウントアップ
		else if(mouse[i] > 0)			mouse[i] = -1;//放した瞬間
		else							mouse[i] = 0; //押されてなかったら0
	}
}
int MouseData::Get(int MouseCode) {
	return mouse[MouseCode-1];
}
int MouseData::GetX() {
	return x;
}
int MouseData::GetY() {
	return y;
}
int MouseData::GetWheel() {
	return wheel;
}
void MouseData::PosCorrection() {
	x = pre_x;
	y = pre_y;
}
//////////////////////////////////////////////標準入力////////////////////////////////////////////////////
/*void InputStr::Input() {
	// 枠
	DrawBox(790, 490, 1410, 710, WHITE, true);
	DrawBox(800, 500, 1400, 700, BLACK, true);

	DrawString(810, 510, str.c_str(), WHITE);
	DrawString(810, 535, "255文字まで", WHITE);
	DrawString(1240, 675, "ESCで入力取り消し", WHITE);
	// 標準入力
	if (KeyInputString(810, 560, 255, tmp, true) == 1) {
		// 無は入れない
		if (tmp[0] != '\0') {
			// 受け取ったデータを代入
			*destination = tmp;
		}
	}
}*/
/*void InputNum::Input() {
	// 枠
	DrawBox(790, 490, 1410, 710, WHITE, true);
	DrawBox(800, 500, 1400, 700, BLACK, true);

	DrawString(810, 510, str.c_str(), WHITE);
	DrawString(810, 535, "-16385〜16384まで", WHITE);
	DrawString(1240, 675, "ESCで入力取り消し", WHITE);
	// 標準入力
	int tmp = KeyInputNumber(810, 560, 16384, -16385, true);
	if (tmp >= -16385 && tmp <= 16384) {
		// 受け取ったデータを代入
		*destination = tmp;
	}
}*/
InputStr::InputStr(std::string* destination, std::string str) :
	destination(destination), 
	InputData(str) 
{
	// キー入力ハンドル作成
	handle = MakeKeyInput(158, true, false, false);
	SetActiveKeyInput(handle);
	SetKeyInputString((*destination).c_str(), handle);
}
bool InputStr::UpDate(){
	switch (CheckKeyInput(handle)){
	case 0://【入力中】
		return false;
	case 1://【入力終了】
		char tmp[158];
		// 受け取ったデータを代入
		if (GetKeyInputString(tmp, handle) != -1) {

		}
		*destination = tmp;

		return true;
	case 2://【キャンセル】
		return  true;
	default://【エラー】
		return  true;
		break;
	}
}
void InputStr::Draw() {
	// 枠
	DrawBox(790, 490, 1410, 710, WHITE, true);
	DrawBox(800, 500, 1400, 700, BLACK, true);

	DrawString(810, 510, str.c_str(), WHITE);
	DrawString(810, 535, "158文字まで", WHITE);
	DrawString(1240, 675, "ESCで入力取り消し", WHITE);

	DrawKeyInputModeString(810, 675);	 // 入力モードを描画
	DrawKeyInputString(810, 560, handle);
}
InputNum::InputNum(int* destination, std::string str) :
	destination(destination),
	InputData(str) 
{
	// キー入力ハンドル作成
	handle = MakeKeyInput(10, true, true, true);
	SetActiveKeyInput(handle);
	SetKeyInputString(to_string(*destination).c_str(), handle);	
}
bool InputNum::UpDate() {
	switch (CheckKeyInput(handle)) {
	case 0://【入力中】
		return false;
	case 1://【入力終了】
		// 受け取ったデータを代入
		*destination = GetKeyInputNumber(handle);
		return true;
	case 2://【キャンセル】
		return  true;
	default://【エラー】
		return  true;
		break;
	}
}
void InputNum::Draw() {
	// 枠
	DrawBox(790, 490, 1410, 710, WHITE, true);
	DrawBox(800, 500, 1400, 700, BLACK, true);

	DrawString(810, 510, str.c_str(), WHITE);
	DrawString(810, 535, "-16385〜16384まで", WHITE);
	DrawString(1240, 675, "ESCで入力取り消し", WHITE);

	DrawKeyInputModeString(810, 675);	 // 入力モードを描画
	DrawKeyInputString(810, 560, handle);
}