#pragma once
#include <string>
#include "DxLib.h"
#include "Font.h"
#include <vector>

enum class eCheckHit {
	Hit,
	End,
	Through
};

class Panel {
protected:
	const int x;			// 座標
	const int y;
	const int x2;			// 右下座標
	const int y2;
	const std::string name;	// 名前

	// 【画像】
	static int gr_panel;	// パネル

public:
	Panel(int x, int y, int x2, int y2, std::string name);
	virtual ~Panel();

	static void Init();
	static void Fina();

	virtual bool UpDate();
	virtual void Draw();
	eCheckHit CheckHit();
	void DrawPanel();
};

// 【flag用のボタン】
class FlagButton : public Panel {
private:
	bool* flag;
	std::string trueStr;
	std::string falseStr;
public:
	FlagButton(int x, int y, int x2, int y2, std::string name, bool* flag, std::string trueStr, std::string falseStr);
	~FlagButton();

	bool UpDate();
	void Draw();
};

// 【単純数値変換のボタン】
class SwitchButton : public Panel {
private:
	int* target;
	const int num;
public:
	SwitchButton(int x, int y, int x2, int y2, std::string name, int* target, int num);
	~SwitchButton();

	bool UpDate();
	void Draw();
};

// 【数値入力のボタン】
class Button_InputNum : public Panel {
private:
	int* target;
	std::string description;	// 説明部
public:
	Button_InputNum(int x, int y, int x2, int y2, std::string name, int* target, std::string description);
	~Button_InputNum();

	bool UpDate();
	void Draw();
};

// 【プルダウンメニュー】
class PulldownMenu_int : public Panel {
private:
	int scroll;
	bool downFlag;
	int* target;
	std::vector<std::string> menu;
public:
	PulldownMenu_int(int x, int y, int x2, int y2, std::string name, int* target, std::vector<std::string> menu);
	~PulldownMenu_int();
	
	bool UpDate();
	void Draw();
};

// 【文字入力のボタン】
class Button_InputStr : public Panel {
private:
	std::string* target;
	std::string description;	// 説明部
public:
	Button_InputStr(int x, int y, int x2, int y2, std::string name, std::string* target, std::string description);
	~Button_InputStr();

	bool UpDate();
	void Draw();
};