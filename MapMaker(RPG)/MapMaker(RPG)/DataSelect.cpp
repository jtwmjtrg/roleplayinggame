#include "DataSelect.h"

DataSelect::DataSelect() {
	endFlag = false;
	nextScene = eScene::S_End;

	gr_Back = LoadGraph("img\\mapCreater\\Back_SelectMap.png");
	
	select = 0;
	mapList = GetFolderName("mapdata");
	selectNum = (int)mapList.size();
	scroll = 0;

	if ((int)mapList.size() < 13) {
		scroll_Max = 0;
	}
	else {
		scroll_Max = ((int)mapList.size() - 12) * 64;
	}

}

DataSelect::~DataSelect() {
	DeleteGraph(gr_Back);
}

void DataSelect::UpDate() {
	//【スクロール】
	scroll -= MouseData::GetWheel() * 50;

	if (scroll < 0) {
		scroll = 0;
	}
	if (scroll > scroll_Max) {
		scroll = scroll_Max;
	}

	// 選択位置計算
	if (MouseData::GetY() > 300) {
		select = (MouseData::GetY() + scroll - 300) / 64;
		if (select < 0 || select >= mapList.size()) {
			select = -1;
		}
	}
	else {
		select = -1;
	}
	
	
	// 決定
	if (MouseData::Get(MOUSE_INPUT_LEFT) == 1 && select != -1) {
		nextScene = eScene::S_MapMaker;
		scenePath = mapList[select];
		endFlag = true;
	}

}

void DataSelect::Draw() {
	// カーソル
	if (select != -1) {
		DrawStringToHandle(130, 310 + select * 64 - scroll, "→", BLACK, Font::Get(eFont::SELECT));
		DrawBox(0, 300 + select * 64 - scroll, 1920, 364 + select * 64 - scroll, SKY, true);
	}
	// 選択肢
	for (int i = 0, n = (int)mapList.size(); i < n; i++) {
		DrawBox(0, 300 + i * 64  - scroll, 1920, 364 + i * 64  - scroll, BLACK, false);
		DrawFormatStringToHandle(210, 310 + i * 64  - scroll, BLACK, Font::Get(eFont::SELECT), "%s", mapList[i].c_str());
	}

	// スクロールバー
	DrawBox(1873, 303, 1915, 1075, BLACK, true);
	if (mapList.size() < 13) {
		DrawBox(1873, 303, 1915, 1075, YELLOW, true);
	}
	else {
		DrawBox(1873, 303 + (scroll * (772.0 / (scroll_Max + 12 * 64))), 1915, 303 + (scroll * (772.0 / (scroll_Max + 12 * 64))) +(772 * (12.0 / mapList.size())), YELLOW, true);
	}

	// 背景
	DrawGraph(0, 0, gr_Back, true);
}

// ディレクトリ内検索（string型vectorで返す）
std::vector < std::string > DataSelect::GetFolderName(std::string dir_Name) {

	HANDLE hFind;	// ハンドル	
	WIN32_FIND_DATA fileData;// ファイル情報の構造体(これにファイル名が入る)
	std::vector<std::string> file_names;	// returnするvector

	//検索するディレクトリの指定
	std::string search_name = dir_Name + "\\*";

	hFind = FindFirstFile(search_name.c_str(), &fileData);	// 検索ハンドルの取得

	// 検索できなければエラーを返す
	if (hFind == INVALID_HANDLE_VALUE) {
		throw std::runtime_error("file not found");
	}

	do {
		// ファイル名が"."か".."でなければ格納
		file_names.push_back(fileData.cFileName);	// ファイル名格納
		if (file_names.back() == "." || file_names.back() == "..") {
			file_names.pop_back();	// 削除
		}
		else {
			file_names.back() = file_names.back().substr(0, file_names.back().find('.', 0));	// 修飾子を外す
		}
		//【fileData.cFileNameは文字列と比較できないっぽい】
		/*
		if (fileData.cFileName != "." && fileData.cFileName != "..") {
		file_names.push_back(fileData.cFileName);	// ファイル名格納
		}
		*/

	} while (FindNextFile(hFind, &fileData));	// 次のファイルを検索

	FindClose(hFind);	// 検索終了

	return file_names;
}

std::string DataSelect::GetDataPath() {
	return scenePath;
}