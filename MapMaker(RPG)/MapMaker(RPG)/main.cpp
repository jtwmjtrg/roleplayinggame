#include "Manager.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	ChangeWindowMode(true);
	SetGraphMode(1920, 1080, 32);
	//SetGraphMode(640, 480, 32);
	// ＤＸライブラリ初期化処理
	if (DxLib_Init() == -1)
	{
		return -1;        // エラーが起きたら直ちに終了
	}
	SetBackgroundColor(220, 220, 220);	// 背景色
	SetTransColor(0, 0, 1);		// 透過色
	SetDrawScreen(DX_SCREEN_BACK);
	SetMouseDispFlag(true);			// マウスカーソル表示

	Font::SetFontData();
	PadData::SetPadNum();
	Manager *manager = new Manager();

	while (ProcessMessage() != -1 && !manager->GetEndFlag() &&
		!KeyData::CheckEnd() && PadData::CheckEnd()) {//画面更新 & メッセージ処理 & 画面消去
		ClearDrawScreen();

		KeyData::UpDate();
		PadData::UpDate();
		MouseData::UpDate();

		manager->UpDate();
		manager->Draw();
		ScreenFlip();
	}
	delete manager;

	DxLib_End(); // DXライブラリ終了処理
	return 0;
}