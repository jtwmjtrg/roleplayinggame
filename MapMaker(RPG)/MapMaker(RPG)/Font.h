#pragma once
#include "DxLib.h"

enum class eFont : __int8 { SELECT, ID, UNIT, UNIT2 };

// よく使う色
const int WHITE = GetColor(255, 255, 255);
const int BLACK = GetColor(0, 0, 0);
const int GRAY = GetColor(125, 125, 125);
const int YELLOW = GetColor(255, 255, 0);
const int RED = GetColor(255, 0, 0);
const int BLUE = GetColor(0, 0, 255);
const int GREEN = GetColor(0, 255, 0);
const int SKY = GetColor(175, 175, 255);

class Font {
private:
	static int SELECT;
	static int ID;
	static int UNIT;
	static int UNIT2;

public:
	Font();
	~Font();

	static void SetFontData();

	static int Get(eFont type);
};