#include "Unit.h"
#include "MapMaker.h"

int UnitData::gr_Mode;
int UnitData::gr_Move;
int UnitData::gr_If;
int UnitData::gr_Act;
int UnitData::gr_Editing_Act_Warp;
int UnitData::gr_Editing_Act_Talk1;
int UnitData::gr_Editing_Act_Talk2;
int UnitData::gr_Editing_Act_Battle;
int UnitData::gr_Editing_Act_Damage1; 
int UnitData::gr_Editing_Act_Damage_Target;
int UnitData::gr_Editing_Act_Recovery;
int UnitData::gr_Editing_Act_Item;
int UnitData::gr_Editing_Act_Shop;
int UnitData::gr_Editing_Act_Existence;
int UnitData::gr_Editing_Act_ChangeMode;
int UnitData::gr_Editing_Act_Message;
int UnitData::gr_Editing_Act_SelectBranch1;
int UnitData::gr_Editing_Act_SelectBranch2;
int UnitData::gr_Editing_Act_End;
int UnitData::gr_Editing_Act_Goto;
int UnitData::gr_Editing_Act_GameOver;
int UnitData::gr_Editing_Act_GameClear;
// 挙動の実行条件
int UnitData::gr_Editing_If_Talk;
int UnitData::gr_Editing_If_Touch;
int UnitData::gr_Editing_If_Message;
int UnitData::gr_Editing_If_Always;
int UnitData::gr_Editing_If_Distance;
// 移動
int UnitData::gr_Editing_Move_None;
int UnitData::gr_Editing_Move_Random;
// モード
int UnitData::gr_Editing_Mode_None;
int UnitData::gr_Editing_Mode_Chapter;
int UnitData::gr_Editing_Mode_Chapter2;
// 種類変更時のリスト
int UnitData::gr_ModeList;		// モードリスト
int UnitData::gr_MoveList;		// 移動リスト
int UnitData::gr_IfList;			// 挙動の実行条件リスト
int UnitData::gr_ActList;		// 挙動リスト

int UnitData::gr_Add_Des;		// 追加,削除ボタン

void UnitData::Load() {
	// 簡易表示画面の背景
	gr_Mode = LoadGraph("img\\mapCreater\\Mode.png");
	gr_Move = LoadGraph("img\\mapCreater\\Move.png");
	gr_If = LoadGraph("img\\mapCreater\\If.png");
	gr_Act = LoadGraph("img\\mapCreater\\Act.png");
	//【編集画面の背景】
	// 挙動
	gr_Editing_Act_Warp = LoadGraph("img\\mapCreater\\Editing_Act_Warp.png");					// 転移
	gr_Editing_Act_Talk1 = LoadGraph("img\\mapCreater\\Editing_Act_Talk1.png");					// セリフ枠
	gr_Editing_Act_Talk2 = LoadGraph("img\\mapCreater\\Editing_Act_Talk2.png");					// セリフセリフごとの枠
	gr_Editing_Act_Battle = LoadGraph("img\\mapCreater\\Editing_Act_Battle.png");				// 戦闘	
	gr_Editing_Act_Damage1 = LoadGraph("img\\mapCreater\\Editing_Act_Damage1.png");				// ダメージ枠
	gr_Editing_Act_Damage_Target = LoadGraph("img\\mapCreater\\Editing_Act_Damage_Target.png");	// ダメージ	対象のプルダウンメニュー
	gr_Editing_Act_Recovery = LoadGraph("img\\mapCreater\\Editing_Act_Recovery.png");			// 回復
	gr_Editing_Act_Item = LoadGraph("img\\mapCreater\\Editing_Act_Item.png");					// アイテム
	gr_Editing_Act_Shop = LoadGraph("img\\mapCreater\\Editing_Act_Shop.png");					// 店
	gr_Editing_Act_Existence = LoadGraph("img\\mapCreater\\Editing_Act_Existence.png");			// 出現-消滅
	gr_Editing_Act_ChangeMode = LoadGraph("img\\mapCreater\\Editing_Act_ChangeMode.png");		// モード変更
	gr_Editing_Act_Message = LoadGraph("img\\mapCreater\\Editing_Act_Message.png");				// メッセージ発信
	gr_Editing_Act_SelectBranch1 = LoadGraph("img\\mapCreater\\Editing_Act_SelectBranch1.png");	// 選択分岐枠
	gr_Editing_Act_SelectBranch2 = LoadGraph("img\\mapCreater\\Editing_Act_SelectBranch2.png");	// 選択分岐選択肢ごとの枠
	gr_Editing_Act_End = LoadGraph("img\\mapCreater\\Editing_Act_End.png");						// 強制終了
	gr_Editing_Act_Goto = LoadGraph("img\\mapCreater\\Editing_Act_Goto.png");					// 挙動のジャンプ
	gr_Editing_Act_GameOver = LoadGraph("img\\mapCreater\\Editing_Act_GameOver.png");			// ゲームオーバー
	gr_Editing_Act_GameClear = LoadGraph("img\\mapCreater\\Editing_Act_GameClear.png");			// ゲームクリア
	// 挙動の実行条件
	gr_Editing_If_Talk = LoadGraph("img\\mapCreater\\Editing_If_Talk.png");			// 話しかける
	gr_Editing_If_Touch = LoadGraph("img\\mapCreater\\Editing_If_Touch.png");		// 触れる
	gr_Editing_If_Message = LoadGraph("img\\mapCreater\\Editing_If_Message.png");	// メッセージ受信
	gr_Editing_If_Always = LoadGraph("img\\mapCreater\\Editing_If_Always.png");		// 常時
	gr_Editing_If_Distance = LoadGraph("img\\mapCreater\\Editing_If_Distance.png");	// プレイヤーとの距離判定
	// 移動
	gr_Editing_Move_None = LoadGraph("img\\mapCreater\\Editing_Move_None.png");		// 移動なし
	gr_Editing_Move_Random = LoadGraph("img\\mapCreater\\Editing_Move_Random.png");	// ランダム移動
	// モード
	gr_Editing_Mode_None = LoadGraph("img\\mapCreater\\Editing_Mode_None.png");		// 条件なし
	gr_Editing_Mode_Chapter = LoadGraph("img\\mapCreater\\Editing_Mode_Chapter.png");	// ストーリー進行度（枠）
	gr_Editing_Mode_Chapter2 = LoadGraph("img\\mapCreater\\Editing_Mode_Chapter2.png");	// ストーリー進行度（条件のプルダウンメニュー）
	// 種類変更時のリスト
	gr_ModeList = LoadGraph("img\\mapCreater\\ModeList.png");			// モードリスト
	gr_MoveList = LoadGraph("img\\mapCreater\\MoveList.png");			// 移動リスト
	gr_IfList = LoadGraph("img\\mapCreater\\IfList.png");				// 挙動の実行条件リスト
	gr_ActList = LoadGraph("img\\mapCreater\\ActList.png");				// 挙動リスト
	
	gr_Add_Des = LoadGraph("img\\mapCreater\\Add_Des.png");				// 追加,削除ボタン
}
void UnitData::Release() {
	DeleteGraph(gr_Mode);
	DeleteGraph(gr_Move);
	DeleteGraph(gr_If);
	DeleteGraph(gr_Act);
	// 挙動
	DeleteGraph(gr_Editing_Act_Warp);			// 転移
	DeleteGraph(gr_Editing_Act_Talk1);			// セリフ枠
	DeleteGraph(gr_Editing_Act_Talk2);			// セリフセリフごとの枠
	DeleteGraph(gr_Editing_Act_Battle);			// 戦闘	
	DeleteGraph(gr_Editing_Act_Damage1);		// ダメージ枠
	DeleteGraph(gr_Editing_Act_Damage_Target);	// ダメージ	対象のプルダウンメニュー
	DeleteGraph(gr_Editing_Act_Recovery );		// 回復
	DeleteGraph(gr_Editing_Act_Item);			// アイテム
	DeleteGraph(gr_Editing_Act_Shop);			// 店
	DeleteGraph(gr_Editing_Act_Existence);		// 出現-消滅
	DeleteGraph(gr_Editing_Act_ChangeMode);		// モード変更
	DeleteGraph(gr_Editing_Act_Message);		// モード変更
	DeleteGraph(gr_Editing_Act_SelectBranch1);	// 選択分岐枠
	DeleteGraph(gr_Editing_Act_SelectBranch2);	// 選択分岐選択肢ごとの枠
	DeleteGraph(gr_Editing_Act_End);			// 強制終了
	DeleteGraph(gr_Editing_Act_Goto);			// 挙動のジャンプ
	DeleteGraph(gr_Editing_Act_GameOver);		// ゲームオーバー
	DeleteGraph(gr_Editing_Act_GameClear);		// ゲームクリア
	// 挙動の実行条件
	DeleteGraph(gr_Editing_If_Talk);		// 話しかける
	DeleteGraph(gr_Editing_If_Touch);		// 触れる
	DeleteGraph(gr_Editing_If_Message);		// メッセージ受信
	DeleteGraph(gr_Editing_If_Always);		// 常時
	DeleteGraph(gr_Editing_If_Distance);	// プレイヤーとの距離判定
	// 移動
	DeleteGraph(gr_Editing_Move_None);		// 移動なし
	DeleteGraph(gr_Editing_Move_Random);	// ランダム移動
	// モード
	DeleteGraph(gr_Editing_Mode_None);		// 条件なし
	DeleteGraph(gr_Editing_Mode_Chapter);	// ストーリー進行度（枠）
	DeleteGraph(gr_Editing_Mode_Chapter2);	// ストーリー進行度（条件のプルダウンメニュー）
	// 種類変更時のリスト
	DeleteGraph(gr_ModeList);		// モードリスト
	DeleteGraph(gr_MoveList);		// 移動リスト
	DeleteGraph(gr_IfList);			// 挙動の実行条件リスト
	DeleteGraph(gr_ActList);		// 挙動リスト

	DeleteGraph(gr_Add_Des);		// 追加,削除ボタン
}

UnitData::UnitData() {
	ChangeTypeFlag = false;	// 種類の変更フラグ
}
UnitData::~UnitData() {
	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		delete panel[i];
	}
	panel.clear();
	panel.shrink_to_fit();
}
bool UnitData::Editing() {
	bool endFlag = true;

	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		if (!panel[i]->UpDate()) {
			endFlag = false;
		}
	}

	if (change_detailsType != detailsType) {
		MapMaker::changeUnitDataWindow.flag = true;
		MapMaker::changeUnitDataWindow.selectTypeList = change_detailsType;
		MapMaker::changeUnitDataWindow.unitDataType = (eUnitData)dataType;
		change_detailsType = detailsType;
	}

	return endFlag;
}
void UnitData::DrawData() {
	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		panel[i]->Draw();
	}
}

//【非対応データ】
UnitData_Default::UnitData_Default(string line) : UnitData() {
	this->line = line + "\n";
}
UnitData_Default::~UnitData_Default() {
	// 特になし
}
bool UnitData_Default::Editing() {
	return true;
}
void UnitData_Default::DrawData() {
	
}
void UnitData_Default::DrawSimple(int y) {
	DrawString(1548, y + 8, "非対応データ", RED);
}
std::string UnitData_Default::GetCsv() {
	return line;
}

/*-------------------------------------------------ユニットの挙動データ--------------------------------------------*/
MapUnit_Action::MapUnit_Action() : UnitData() {
	std::vector<std::string> tmp_menu = {
		"転移",
		"セリフ",
		"戦闘",
		"ダメージ",
		"回復",
		"アイテム取得",
		"店",
		"消滅-出現",
		"モード変更",
		"メッセージ",
		"選択分岐",
		"強制終了",
		"挙動番号の移動",
		"強制ゲームオーバー",
		"ゲームクリア",
		"金を払う"
	};
	panel.push_back(new PulldownMenu_int(300, 200, 600, 300, "挙動の種類", &change_detailsType, tmp_menu));
	dataType = 3;
}
void MapUnit_Action::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "挙動", BLUE);
}
//【転移】
MapUnit_Action_Warp::MapUnit_Action_Warp() : MapUnit_Action() {
	mapId = 0;	// 転移先のマップID
	x = 0;		// 転移先の座標
	y = 0;
	effect = 0;	// 転移エフェクトの種類

	detailsType = 0;
	change_detailsType = detailsType;

	SetPanel();
}
MapUnit_Action_Warp::MapUnit_Action_Warp(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【マップID】取得
	std::getline(L_stream, L_token, ',');
	this->mapId = boost::lexical_cast<int>(L_token);
	// 3【X座標】取得（マップ座標）
	std::getline(L_stream, L_token, ',');
	this->x = boost::lexical_cast<int>(L_token);
	// 4【Y座標】取得（マップ座標）
	std::getline(L_stream, L_token, ',');
	this->y = boost::lexical_cast<int>(L_token);
	// 5【転移演出】取得
	std::getline(L_stream, L_token, ',');
	this->effect = boost::lexical_cast<int>(L_token);
	
	SetPanel();
}
MapUnit_Action_Warp::~MapUnit_Action_Warp() {
	// 特になし
}
void MapUnit_Action_Warp::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 800, 300, "転移先のマップID", &mapId, "転移先のマップのIDを入力してください"));
	panel.push_back(new Button_InputNum(800, 200, 1000, 300, "X座標", &x, "X座標を入力してください"));
	panel.push_back(new Button_InputNum(1000, 200, 1200, 300, "Y座標", &y, "Y座標を入力してください"));
	panel.push_back(new Button_InputNum(1200, 200, 1400, 300, "演出ID", &effect, "演出IDを入力してください"));
}
void MapUnit_Action_Warp::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "転移", BLUE);
}
std::string MapUnit_Action_Warp::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "0,";		// 挙動の種類
	csv += to_string(mapId) + ',';		// 転移先マップのID
	csv += to_string(x) + ',';			// 座標
	csv += to_string(y) + ',';
	csv += to_string(effect) + ',';		// 演出
	csv += '\n';
	return csv;
}
//【セリフ】
MapUnit_Action_Talk::MapUnit_Action_Talk() : MapUnit_Action() {
	str.push_back("####セリフを入力してください####");
	detailsType = 1;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_Talk::MapUnit_Action_Talk(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【セリフ内容】取得
	while (std::getline(L_stream, L_token, ',')) {
		if (L_token == "") break;
		this->str.push_back(L_token);
	}
	SetPanel();
}
MapUnit_Action_Talk::~MapUnit_Action_Talk() {
	for (int i = 0, n = (int)serifPanel.size(); i < n; ++i) {
		delete serifPanel[i];
	}
	serifPanel.clear();
	serifPanel.shrink_to_fit();
}
void MapUnit_Action_Talk::SetPanel() {
	for (int i = 0, n = (int)str.size(); i < n; ++i) {
		serifPanel.push_back(new Button_InputStr(600, 200 + i * 48, 1400, 248 + i * 48, "", &(str[i]), "セリフを入力してください"));
	}
}
bool MapUnit_Action_Talk::Editing() {
	bool endFlag = true;

	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		if (!panel[i]->UpDate()) {
			endFlag = false;
		}
	}
	for (int i = 0, n = (int)serifPanel.size(); i < n; ++i) {
		if (!serifPanel[i]->UpDate()) {
			endFlag = false;
		}
	}

	if (MouseData::Get(MOUSE_INPUT_LEFT) == 1 && MouseData::GetX() > 1400 && MouseData::GetX() <= 1496 && MouseData::GetY() >= 200 && MouseData::GetY() <= 200 + (str.size() * 48)) {
		int tmp_selectN = (MouseData::GetY() - 200) / 48;
		//【セリフ追加】
		if (MouseData::GetX() < 1448) {
			str.insert(str.begin() + tmp_selectN + 1, "####セリフを入力してください####");
			for (int i = 0, n = (int)serifPanel.size(); i < n; ++i) {
				delete serifPanel[i];
			}
			serifPanel.clear();
			serifPanel.shrink_to_fit();
			for (int i = 0, n = (int)str.size(); i < n; ++i) {
				serifPanel.push_back(new Button_InputStr(600, 200 + i * 48, 1400, 248 + i * 48, "", &(str[i]), "セリフを入力してください"));
			}
		}
		//【セリフ削除】
		else {
			// サイズが0にならないように
			if (str.size() > 1) {
				str.erase(str.begin() + tmp_selectN);
			}
			for (int i = 0, n = (int)serifPanel.size(); i < n; ++i) {
				delete serifPanel[i];
			}
			serifPanel.clear();
			serifPanel.shrink_to_fit();
			for (int i = 0, n = (int)str.size(); i < n; ++i) {
				serifPanel.push_back(new Button_InputStr(600, 200 + i * 48, 1400, 248 + i * 48, "", &(str[i]), "セリフを入力してください"));
			}
		}
		endFlag = false;
	}
	

	if (change_detailsType != detailsType) {
		MapMaker::changeUnitDataWindow.flag = true;
		MapMaker::changeUnitDataWindow.selectTypeList = change_detailsType;
		MapMaker::changeUnitDataWindow.unitDataType = (eUnitData)dataType;
		change_detailsType = detailsType;
	}

	return endFlag;
}
void MapUnit_Action_Talk::DrawData() {
	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		panel[i]->Draw();
	}
	for (int i = 0, n = (int)serifPanel.size(); i < n; ++i) {
		serifPanel[i]->Draw();
		DrawGraph(1400, 200 + i * 48, gr_Add_Des, false);
	}
}
void MapUnit_Action_Talk::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "セリフ", BLUE);
	DrawString(1630, y + 8, str[0].c_str(), BLACK);	// 最初の1行のみ表示
}
std::string MapUnit_Action_Talk::GetCsv() {
	string csv = "3,";	// データの種類
	csv += "1,";		// 挙動の種類
	// セリフ
	for (int i = 0, n = (int)str.size(); i < n; i++) {
		csv += str[i] + ',';
	}
	csv += '\n';
	return csv;
}
//【戦闘】
MapUnit_Action_Battle::MapUnit_Action_Battle() : MapUnit_Action() {
	encID = 0;					// エンカウントID
	backID = 0;					// 背景ID
	bgmPath = "bossbattle.wav";	// BGMのパス
	escapeFlag = true;			// 逃走可能フラグ
	loseEventFlag = false;		// 負けイベフラグ
	// 戦闘終了の状態で分岐（挙動番号のジャンプ数）
	branch[0] = 1;
	branch[1] = 1;
	branch[2] = 1;

	detailsType = 2;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_Battle::MapUnit_Action_Battle(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【エンカウントID】取得
	std::getline(L_stream, L_token, ',');
	encID = boost::lexical_cast<int>(L_token);
	// 4【背景ID】取得
	std::getline(L_stream, L_token, ',');
	backID = boost::lexical_cast<int>(L_token);
	// 【BGMのパス】
	std::getline(L_stream, L_token, ',');
	bgmPath = L_token;
	// 5【逃走可能フラグ】取得
	std::getline(L_stream, L_token, ',');
	escapeFlag = L_token == "1";
	// 6【負けイベフラグ】取得
	std::getline(L_stream, L_token, ',');
	loseEventFlag = L_token == "1";
	// 7【勝利時の分岐】取得
	std::getline(L_stream, L_token, ',');
	branch[0] = boost::lexical_cast<int>(L_token);
	// 8【全滅時の分岐】取得
	std::getline(L_stream, L_token, ',');
	branch[1] = boost::lexical_cast<int>(L_token);
	// 9【逃走時の分岐】取得
	std::getline(L_stream, L_token, ',');
	branch[2] = boost::lexical_cast<int>(L_token);
	
	SetPanel();
}
MapUnit_Action_Battle::~MapUnit_Action_Battle() {
	// 特になし
}
void MapUnit_Action_Battle::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 800, 300, "エンカウントID", &encID, "エンカウントIDを入力してください"));
	panel.push_back(new Button_InputNum(800, 200, 1000, 300, "背景ID", &backID, "背景IDを入力してください"));
	panel.push_back(new Button_InputStr(1000, 200, 1400, 300, "BGMのパス", &bgmPath, "BGMのパスを入力してください"));
	panel.push_back(new Button_InputNum(800, 300, 1000, 400, "勝利時の分岐", &branch[0], "勝利時のジャンプ数を入力してください"));
	panel.push_back(new Button_InputNum(1000, 300, 1200, 400, "全滅時の分岐", &branch[1], "全滅時のジャンプ数を入力してください"));
	panel.push_back(new Button_InputNum(1200, 300, 1400, 400, "逃走時の分岐", &branch[2], "逃走時のジャンプ数を入力してください"));
}
void MapUnit_Action_Battle::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "戦闘", BLUE);
	DrawFormatString(1630, y + 8, BLACK, "%d", encID);	// エンカウントID
	if (!escapeFlag) {
		DrawString(1700, y + 8, "逃走不可", BLACK);
	}
	if (loseEventFlag) {
		DrawString(1800, y + 8, "負けイベ", BLACK);
	}
}
std::string MapUnit_Action_Battle::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "2,";		// 挙動の種類
	csv += to_string(encID) + ',';		// エンカウントID
	csv += to_string(backID) + ',';		// 背景ID
	csv += bgmPath + ',';				// BGMのパス
	csv += (escapeFlag ? "1," : "0,");	// 逃走可能フラグ
	csv += (loseEventFlag ? "1," : "0,");	// 負けイベフラグ
	csv += to_string(branch[0]) + ',';		// 勝利	
	csv += to_string(branch[1]) + ',';		// 敗北
	csv += to_string(branch[2]) + ',';		// 逃走	
	csv += '\n';
	return csv;
}
//【ダメージ】
MapUnit_Action_Damage::MapUnit_Action_Damage() : MapUnit_Action() {
	damage = 0;	// ダメージ量（固定ダメージ）
	target = 0;	// 対象	0:全員	1:先頭	2:ランダム
	
	detailsType = 3;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_Damage::MapUnit_Action_Damage(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【対象】取得
	std::getline(L_stream, L_token, ',');
	target = boost::lexical_cast<int>(L_token);
	// 4【ダメージ量】取得
	std::getline(L_stream, L_token, ',');
	damage = boost::lexical_cast<int>(L_token);
	
	SetPanel();
}
MapUnit_Action_Damage::~MapUnit_Action_Damage() {
	// 特になし
}
void MapUnit_Action_Damage::SetPanel() {
	std::vector<std::string> tmp_menu = {
		"全員",
		"先頭",
		"ランダム1人"
	};
	panel.push_back(new PulldownMenu_int(400, 200, 600, 300, "対象", &target, tmp_menu));
	panel.push_back(new Button_InputNum(800, 200, 1000, 300, "ダメージ量", &damage, "ダメージ量を入力してください"));
}
void MapUnit_Action_Damage::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "ダメージ", BLUE);
	// 対象
	switch (target){
	case 0:
		DrawString(1630, y + 8, "全員に", BLACK);
		break;
	case 1:
		DrawString(1630, y + 8, "先頭に", BLACK);
		break;
	case 2:
		DrawString(1630, y + 8, "ﾗﾝﾀﾞﾑに", BLACK);
		break;
	default:
		DrawString(1630, y + 8, "エラー", BLACK);
		break;
	}
	DrawFormatString(1700, y + 8, BLACK, "%dダメージ", damage);	// ダメージ数	
}
std::string MapUnit_Action_Damage::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "3,";		// 挙動の種類
	csv += to_string(target) + ',';		// 
	csv += to_string(damage) + ',';		// 
	csv += '\n';
	return csv;
}
//【回復】
MapUnit_Action_Recovery::MapUnit_Action_Recovery() : MapUnit_Action() {
	heal_HP = 0;			// HP回復量（固定量）	-1:全快
	heal_MP = 0;			// MP回復量（固定量）	-1:全快
	resurrection = false;	// 蘇生フラグ
	target = 0;				// 対象	0:全員	1:先頭	2:ランダム

	detailsType = 4;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_Recovery::MapUnit_Action_Recovery(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【対象】取得
	std::getline(L_stream, L_token, ',');
	target = boost::lexical_cast<int>(L_token);
	// 4【HP回復量】取得
	std::getline(L_stream, L_token, ',');
	heal_HP = boost::lexical_cast<int>(L_token);
	// 5【MP回復量】取得
	std::getline(L_stream, L_token, ',');
	heal_MP = boost::lexical_cast<int>(L_token);
	// 6【蘇生フラグ】取得
	std::getline(L_stream, L_token, ',');
	resurrection = L_token == "1";

	SetPanel();
}
MapUnit_Action_Recovery::~MapUnit_Action_Recovery() {
	// 特になし
}
void MapUnit_Action_Recovery::SetPanel() {
	std::vector<std::string> tmp_menu = {
		"全員",
		"先頭",
		"ランダム1人"
	};
	panel.push_back(new PulldownMenu_int(600, 200, 800, 300, "対象", &target, tmp_menu));
	panel.push_back(new Button_InputNum(800, 200, 1000, 300, "HP回復量", &heal_HP, "HP回復量を入力してください"));
	panel.push_back(new Button_InputNum(1000, 200, 1200, 300, "MP回復量", &heal_MP, "MP回復量を入力してください"));
	panel.push_back(new FlagButton(1200, 200, 1400, 300, "蘇生フラグ", &resurrection, "蘇生あり", "蘇生なし"));
}
void MapUnit_Action_Recovery::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "回復", BLUE);
	// 対象
	switch (target) {
	case 0:
		DrawString(1600, y + 8, "全員が", BLACK);
		break;
	case 1:
		DrawString(1600, y + 8, "先頭が", BLACK);
		break;
	case 2:
		DrawString(1600, y + 8, "ﾗﾝﾀﾞﾑに", BLACK);
		break;
	default:
		DrawString(1600, y + 8, "エラー", BLACK);
		break;
	}
	// HP回復量
	switch (heal_HP) {
	case 0:
		// HP回復なし
		break;
	case -1:
		DrawString(1670, y + 8, "HP全快", BLACK);
		break;
	default:
		DrawFormatString(1670, y + 8, BLACK, "HP%d", heal_HP);
		break;
	}
	// MP回復量
	switch (heal_MP) {
	case 0:
		// HP回復なし
		break;
	case -1:
		DrawString(1730, y + 8, "MP全快", BLACK);
		break;
	default:
		DrawFormatString(1730, y + 8, BLACK, "MP%d", heal_MP);
		break;
	}
	if (resurrection) {
		DrawString(1790, y + 8, "蘇生", BLACK);
	}
}
std::string MapUnit_Action_Recovery::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "4,";		// 挙動の種類
	csv += to_string(target) + ',';		// 
	csv += to_string(heal_HP) + ',';	// 
	csv += to_string(heal_MP) + ',';	// 
	csv += (resurrection ? "1," : "0,");	// 
	csv += '\n';
	return csv;
}
//【アイテム取得】
MapUnit_Action_Item::MapUnit_Action_Item() : MapUnit_Action() {
	itemID = 0;		// 取得アイテムのID
	jump_True = 1;	// アイテムを手に入れた場合のジャンプ
	jump_False = 1;	// 持ち物がいっぱいだった場合のジャンプ

	detailsType = 5;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_Item::MapUnit_Action_Item(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【取得アイテムのID】取得
	std::getline(L_stream, L_token, ',');
	itemID = boost::lexical_cast<int>(L_token);
	// 4【アイテムを手に入れた場合のジャンプ】取得
	std::getline(L_stream, L_token, ',');
	jump_True = boost::lexical_cast<int>(L_token);
	// 5【持ち物がいっぱいだった場合のジャンプ】取得
	std::getline(L_stream, L_token, ',');
	jump_False = boost::lexical_cast<int>(L_token);

	SetPanel();
}
MapUnit_Action_Item::~MapUnit_Action_Item() {
	// 特になし
}
void MapUnit_Action_Item::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 900, 300, "取得アイテムのID", &itemID, "取得アイテムのIDを入力してください"));
	panel.push_back(new Button_InputNum(900, 200, 1200, 300, "入手時ジャンプ", &jump_True, "アイテムを手に入れた場合のジャンプ数を入力してください"));
	panel.push_back(new Button_InputNum(1200, 200, 1500, 300, "入手失敗時ジャンプ", &jump_False, "持ち物がいっぱいだった場合のジャンプ数を入力してください"));
}
void MapUnit_Action_Item::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "アイテム", BLUE);
	DrawFormatString(1630, y + 8, BLACK, "%d", itemID);	// アイテムID
}
std::string MapUnit_Action_Item::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "5,";		// 挙動の種類
	csv += to_string(itemID) + ',';		// 
	csv += to_string(jump_True) + ',';		// 
	csv += to_string(jump_False) + ',';		// 
	csv += '\n';
	return csv;
}
//【店】
MapUnit_Action_Shop::MapUnit_Action_Shop() : MapUnit_Action() {
	shopID = "0";		// 店ID

	detailsType = 6;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_Shop::MapUnit_Action_Shop(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【店ID】取得
	std::getline(L_stream, L_token, ',');
	shopID = L_token;

	SetPanel();
}
MapUnit_Action_Shop::~MapUnit_Action_Shop() {
	// 特になし
}
void MapUnit_Action_Shop::SetPanel() {
	panel.push_back(new Button_InputStr(600, 200, 800, 300, "店ID", &shopID, "店IDを入力してください"));
}
void MapUnit_Action_Shop::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "店", BLUE);
	DrawString(1630, y + 8, shopID.c_str(), BLACK);	// 店ID
}
std::string MapUnit_Action_Shop::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "6,";		// 挙動の種類
	csv += shopID + ",";		// 
	csv += '\n';
	return csv;
}
//【消滅-出現】
MapUnit_Action_Existence::MapUnit_Action_Existence() : MapUnit_Action() {
	flag = false;	// true::出現	false:消滅
	staging = 0;	// 演出
}
MapUnit_Action_Existence::MapUnit_Action_Existence(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【フラグ】取得
	std::getline(L_stream, L_token, ',');
	flag = L_token == "1";
	// 4【演出】取得
	std::getline(L_stream, L_token, ',');
	staging = boost::lexical_cast<int>(L_token);
}
MapUnit_Action_Existence::~MapUnit_Action_Existence() {
	// 特になし
}
void MapUnit_Action_Existence::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, (flag ? "出現" : "消滅"), BLUE);
}
std::string MapUnit_Action_Existence::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "7,";		// 挙動の種類
	csv += (flag ? "1," : "0,");	// 
	csv += to_string(staging) + ',';		// 
	csv += '\n';
	return csv;
}
//【モード変更】
MapUnit_Action_ChangeMode::MapUnit_Action_ChangeMode() : MapUnit_Action() {
	modeNum = 0;	// 変更後のモード番号

	detailsType = 8;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_ChangeMode::MapUnit_Action_ChangeMode(string line) : MapUnit_Action() {
	//【データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【モード数】取得
	std::getline(L_stream, L_token, ',');
	modeNum = boost::lexical_cast<int>(L_token);

	panel.push_back(new Button_InputNum(600, 200, 800, 300, "モード数", &modeNum, "モード数を入力してください"));
	SetPanel();
}
MapUnit_Action_ChangeMode::~MapUnit_Action_ChangeMode() {
	// 特になし
}
void MapUnit_Action_ChangeMode::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 800, 300, "モード数", &modeNum, "モード数を入力してください"));
}
void MapUnit_Action_ChangeMode::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "モード変更", BLUE);
	DrawFormatString(1650, y + 8, BLACK, "%d", modeNum);	// モード番号
}
std::string MapUnit_Action_ChangeMode::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "8,";		// 挙動の種類
	csv += to_string(modeNum);	// モード番号
	csv += '\n';
	return csv;
}
//【メッセージ発信】
MapUnit_Action_Message::MapUnit_Action_Message() : MapUnit_Action() {
	messageNum = 0;	// メッセージ番号

	detailsType = 9;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_Message::MapUnit_Action_Message(string line) : MapUnit_Action() {
	//【データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【メッセージ番号】取得
	std::getline(L_stream, L_token, ',');
	messageNum = boost::lexical_cast<int>(L_token);

	SetPanel();
}
MapUnit_Action_Message::~MapUnit_Action_Message() {
	// 特になし
}
void MapUnit_Action_Message::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 900, 300, "メッセージ番号", &messageNum, "メッセージ番号を入力してください"));
}
void MapUnit_Action_Message::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "メッセージ発信", BLUE);
	DrawFormatString(1700, y + 8, BLACK, "%d", messageNum);	// メッセージ番号
}
std::string MapUnit_Action_Message::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "9,";		// 挙動の種類
	csv += to_string(messageNum);	// モード番号
	csv += '\n';
	return csv;
}
//【選択分岐】
MapUnit_Action_SelectBranch::MapUnit_Action_SelectBranch() : MapUnit_Action() {
	question = "####質問文を入力してください####";	// 質問文
	choices.push_back({ "####選択肢文を入力してください####", 0 });	// 選択肢

	detailsType = 10;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_SelectBranch::MapUnit_Action_SelectBranch(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【質問文】取得
	std::getline(L_stream, L_token, ',');
	question = L_token;
	// 【選択肢】取得
	while (std::getline(L_stream, L_token, ',')) {
		if (L_token == "") break;
		// 【選択肢文】取得
		choices.push_back({ L_token, 1 });
		// 【分岐先】取得
		std::getline(L_stream, L_token, ',');
		choices.back().destination = boost::lexical_cast<int>(L_token);
	}
	
	SetPanel();
}
MapUnit_Action_SelectBranch::~MapUnit_Action_SelectBranch() {
	// 特になし
}
void MapUnit_Action_SelectBranch::SetPanel() {
	panel.push_back(new Button_InputStr(600, 200, 1400, 300, "質問文", &question, "質問文を入力してください"));
	panel.push_back(new Panel(600, 300, 800, 348, "分岐"));
	panel.push_back(new Panel(600, 300, 800, 348, "選択肢文"));
	panel.push_back(new Panel(1300, 300, 1400, 348, "分岐先"));
	for (int i = 0, n = (int)choices.size(); i < n; ++i) {
		choicesPanel.push_back(new Button_InputStr(600, 348 + i * 48, 1300, 348 + (i + 1) * 48, "", &(choices[i].answer), "選択肢文を入力してください"));
		choicesPanel.push_back(new Button_InputNum(1300, 348 + i * 48, 1400, 348 + (i + 1) * 48, "", &(choices[i].destination), "分岐先を入力してください"));
	}
}
bool MapUnit_Action_SelectBranch::Editing() {
	bool endFlag = true;

	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		if (!panel[i]->UpDate()) {
			endFlag = false;
		}
	}
	for (int i = 0, n = (int)choicesPanel.size(); i < n; ++i) {
		if (!choicesPanel[i]->UpDate()) {
			endFlag = false;
		}
	}

	if (MouseData::Get(MOUSE_INPUT_LEFT) == 1 && MouseData::GetX() > 1400 && MouseData::GetX() <= 1496 && MouseData::GetY() >= 348 && MouseData::GetY() <= 348 + (choices.size() * 48)) {
		int tmp_selectN = (MouseData::GetY() - 348) / 48;
		//【セリフ追加】
		if (MouseData::GetX() < 1448) {
			choices.insert(choices.begin() + tmp_selectN + 1, { "####選択肢文を入力してください####", 0 });
			for (int i = 0, n = (int)choicesPanel.size(); i < n; ++i) {
				delete choicesPanel[i];
			}
			choicesPanel.clear();
			choicesPanel.shrink_to_fit();
			for (int i = 0, n = (int)choices.size(); i < n; ++i) {
				choicesPanel.push_back(new Button_InputStr(600, 348 + i * 48, 1300, 348 + (i + 1) * 48, "", &(choices[i].answer), "選択肢文を入力してください"));
				choicesPanel.push_back(new Button_InputNum(1300, 348 + i * 48, 1400, 348 + (i + 1) * 48, "", &(choices[i].destination), "分岐先を入力してください"));
			}
		}
		//【セリフ削除】
		else {
			// サイズが0にならないように
			if (choices.size() > 1) {
				choices.erase(choices.begin() + tmp_selectN);
			}
			for (int i = 0, n = (int)choicesPanel.size(); i < n; ++i) {
				delete choicesPanel[i];
			}
			choicesPanel.clear();
			choicesPanel.shrink_to_fit();
			for (int i = 0, n = (int)choices.size(); i < n; ++i) {
				choicesPanel.push_back(new Button_InputStr(600, 348 + i * 48, 1300, 348 + (i + 1) * 48, "", &(choices[i].answer), "選択肢文を入力してください"));
				choicesPanel.push_back(new Button_InputNum(1300, 348 + i * 48, 1400, 348 + (i + 1) * 48, "", &(choices[i].destination), "分岐先を入力してください"));
			}
		}
		endFlag = false;
	}


	if (change_detailsType != detailsType) {
		MapMaker::changeUnitDataWindow.flag = true;
		MapMaker::changeUnitDataWindow.selectTypeList = change_detailsType;
		MapMaker::changeUnitDataWindow.unitDataType = (eUnitData)dataType;
		change_detailsType = detailsType;
	}

	return endFlag;
}
void MapUnit_Action_SelectBranch::DrawData() {
	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		panel[i]->Draw();
	}
	for (int i = 0, n = (int)choicesPanel.size(); i < n; ++i) {
		choicesPanel[i]->Draw();
		if (i & 1) {
			DrawGraph(1400, 348 + (i / 2) * 48, gr_Add_Des, false);
		}
	}
}
void MapUnit_Action_SelectBranch::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "選択分岐", BLUE);
	DrawString(1630, y + 8, question.c_str(), BLACK);	// 質問文
}
std::string MapUnit_Action_SelectBranch::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "10,";		// 挙動の種類
	csv += question + ',';
	for (int i = 0, n = (int)choices.size(); i < n; ++i) {
		csv += choices[i].answer + ',';
		csv += to_string(choices[i].destination) + ',';
	}
	csv += '\n';
	return csv;
}
//【強制終了】
MapUnit_Action_End::MapUnit_Action_End() : MapUnit_Action() {
	detailsType = 11;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_End::~MapUnit_Action_End() {

}
void MapUnit_Action_End::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "強制終了", BLUE);
}
std::string MapUnit_Action_End::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "11,\n";		// 挙動の種類;
	return csv;
}
//【挙動番号の移動】
MapUnit_Action_Goto::MapUnit_Action_Goto() : MapUnit_Action() {
	moveNum = 1;	// 移動数
	detailsType = 12;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_Goto::MapUnit_Action_Goto(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【ジャンプ数】取得
	std::getline(L_stream, L_token, ',');
	moveNum = boost::lexical_cast<int>(L_token);

	SetPanel();
}
MapUnit_Action_Goto::~MapUnit_Action_Goto() {
	// 特になし
}
void MapUnit_Action_Goto::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 900, 300, "ジャンプ数", &moveNum, "ジャンプ数を入力してください"));
}
void MapUnit_Action_Goto::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "挙動のジャンプ", BLUE);
	DrawFormatString(1730, y + 8, BLACK, "%d", moveNum);	// ジャンプ数
}
std::string MapUnit_Action_Goto::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "12,";		// 挙動の種類
	csv += to_string(moveNum) + ',';
	csv += '\n';
	return csv;
}
//【強制ゲームオーバー】
MapUnit_Action_GameOver::MapUnit_Action_GameOver() : MapUnit_Action() {
	detailsType = 13;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_GameOver::~MapUnit_Action_GameOver() {

}
void MapUnit_Action_GameOver::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "強制ゲームオーバー", BLUE);
}
std::string MapUnit_Action_GameOver::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "13,\n";		// 挙動の種類;
	return csv;
}
//【ゲームクリア】
MapUnit_Action_GameClear::MapUnit_Action_GameClear() : MapUnit_Action() {
	detailsType = 14;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_GameClear::~MapUnit_Action_GameClear() {

}
void MapUnit_Action_GameClear::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "ゲームクリア", BLUE);
}
std::string MapUnit_Action_GameClear::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "14,\n";		// 挙動の種類;
	return csv;
}
//【金を払う】
MapUnit_Action_PayMoney::MapUnit_Action_PayMoney() : MapUnit_Action() {
	price = 0;		// 払う金額
	jump_True = 1;	// 金が足りた場合のジャンプ
	jump_False = 1;	// 金が足りなかった場合のジャンプ

	detailsType = 15;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Action_PayMoney::MapUnit_Action_PayMoney(string line) : MapUnit_Action() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【取得アイテムのID】取得
	std::getline(L_stream, L_token, ',');
	price = boost::lexical_cast<int>(L_token);
	// 4【金が足りた場合のジャンプ】取得
	std::getline(L_stream, L_token, ',');
	jump_True = boost::lexical_cast<int>(L_token);
	// 5【金が足りなかった場合のジャンプ】取得
	std::getline(L_stream, L_token, ',');
	jump_False = boost::lexical_cast<int>(L_token);

	SetPanel();
}
MapUnit_Action_PayMoney::~MapUnit_Action_PayMoney() {
	// 特になし
}
void MapUnit_Action_PayMoney::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 900, 300, "払う金額", &price, "取得アイテムのIDを入力してください"));
	panel.push_back(new Button_InputNum(900, 200, 1200, 300, "金が足りた場合のジャンプ", &jump_True, "金が足りた場合のジャンプ数を入力してください"));
	panel.push_back(new Button_InputNum(1200, 200, 1500, 300, "金が足りなかった場合のジャンプ", &jump_False, "金が足りなかった場合のジャンプ数を入力してください"));
}
void MapUnit_Action_PayMoney::DrawSimple(int y) {
	DrawGraph(1532, y, gr_Act, true);
	DrawString(1548, y + 8, "金を払う", BLUE);
	DrawFormatString(1630, y + 8, BLACK, "%dＧ", price);	// アイテムID
}
std::string MapUnit_Action_PayMoney::GetCsv() {
	string csv = "3,";	// データの種類

	csv += "15,";		// 挙動の種類
	csv += to_string(price) + ',';		// 
	csv += to_string(jump_True) + ',';		// 
	csv += to_string(jump_False) + ',';		// 
	csv += '\n';
	return csv;
}
/*-------------------------------------------------ユニットの挙動条件データ--------------------------------------------*/
MapUnit_If::MapUnit_If() : UnitData() {
	std::vector<std::string> tmp_menu = {
		"話しかける",
		"触れる",
		"メッセージを受信する",
		"常時",
		"主人公との距離判定"
	};
	panel.push_back(new PulldownMenu_int(300, 200, 600, 300, "挙動条件の種類", &change_detailsType, tmp_menu));
	dataType = 2;
}
void MapUnit_If::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawString(1532, y + 8, "条件", BLACK);
}
//【主人公がこのユニットに話しかける】
MapUnit_If_Talk::MapUnit_If_Talk() : MapUnit_If() {
	detailsType = 0;
	change_detailsType = detailsType;
}
MapUnit_If_Talk::~MapUnit_If_Talk() {
	// 特になし
}
void MapUnit_If_Talk::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawString(1532, y + 8, "話しかける", BLUE);
}
std::string MapUnit_If_Talk::GetCsv() {
	string csv = "2,";	// データの種類

	csv += "0,\n";		// 条件の種類
	return csv;
}
//【主人公がこのユニットに触れる】
MapUnit_If_Touch::MapUnit_If_Touch() : MapUnit_If() {
	detailsType = 1 ;
	change_detailsType = detailsType;
}
MapUnit_If_Touch::~MapUnit_If_Touch() {
	// 特になし
}
void MapUnit_If_Touch::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawString(1532, y + 8, "触れる", BLUE);
}
std::string MapUnit_If_Touch::GetCsv() {
	string csv = "2,";	// データの種類

	csv += "1,\n";		// 条件の種類
	return csv;
}
//【メッセージ受信】
MapUnit_If_Message::MapUnit_If_Message() : MapUnit_If() {
	messageNum = 0;
	detailsType = 2;
	change_detailsType = detailsType;

	SetPanel();
}
MapUnit_If_Message::MapUnit_If_Message(string line) : MapUnit_If() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	dataType = boost::lexical_cast<int>(L_token);
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【メッセージ番号】取得
	std::getline(L_stream, L_token, ',');
	messageNum = boost::lexical_cast<int>(L_token);

	SetPanel();
}
MapUnit_If_Message::~MapUnit_If_Message() {
	// 特になし
}
void MapUnit_If_Message::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 900, 300, "メッセージ番号", &messageNum, "メッセージ番号を入力してください"));
}
void MapUnit_If_Message::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawString(1532, y + 8, "メッセージ受信", BLUE);
	DrawFormatString(1700, y + 8, BLACK, "%d", messageNum);	// メッセージ番号
}
std::string MapUnit_If_Message::GetCsv() {
	string csv = "2,";	// データの種類

	csv += "2,";		// 条件の種類
	csv += to_string(messageNum); 		// メッセージ番号
	csv += '\n';
	return csv;
}
//【常時】
MapUnit_If_Always::MapUnit_If_Always() : MapUnit_If() {
	detailsType = 3;
	change_detailsType = detailsType;
}
MapUnit_If_Always::~MapUnit_If_Always() {
	// 特になし
}
void MapUnit_If_Always::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawString(1532, y + 8, "常時", BLUE);
}
std::string MapUnit_If_Always::GetCsv() {
	string csv = "2,";	// データの種類

	csv += "3,\n";		// 条件の種類
	return csv;
}
//【近づくor離れる】
MapUnit_If_Distance::MapUnit_If_Distance() : MapUnit_If() {
	distance = 512;
	In_Out = true;
	detailsType = 4;
	change_detailsType = detailsType;

	SetPanel();
}
MapUnit_If_Distance::MapUnit_If_Distance(string line) : MapUnit_If() {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	dataType = boost::lexical_cast<int>(L_token);
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【距離（半径）】取得
	std::getline(L_stream, L_token, ',');
	distance = boost::lexical_cast<int>(L_token);
	// 3【近づく/離れる】取得
	std::getline(L_stream, L_token, ',');
	In_Out = L_token == "1";

	SetPanel();
}
MapUnit_If_Distance::~MapUnit_If_Distance() {
	// 特になし
}
void MapUnit_If_Distance::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 900, 300, "距離(半径)", &distance, "距離(半径)を入力してください"));
	panel.push_back(new FlagButton(900, 200, 1200, 300, "近づく/離れる", &In_Out, "近づく", "離れる"));
}
void MapUnit_If_Distance::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawFormatString(1532, y + 8,  BLUE, (In_Out ? "%d以上近づく" : "%d以上離れる"), distance);
}
std::string MapUnit_If_Distance::GetCsv() {
	string csv = "2,";	// データの種類

	csv += "4,";		// 条件の種類
	csv += to_string(distance) + ','; 		// 距離（半径）
	csv += (In_Out ?  "1" : "0"); 	// true:近づく	false:離れる
	csv += '\n';
	return csv;
}
/*-------------------------------------------------ユニットの移動データ--------------------------------------------*/
MapUnit_Move::MapUnit_Move() : UnitData() {
	std::vector<std::string> tmp_menu = {
		"動かない",
		"プレイヤーを追う or 逃げる",
		"ループ移動",
		"ランダム"
	};
	panel.push_back(new PulldownMenu_int(300, 200, 600, 300, "移動の種類", &change_detailsType, tmp_menu));
	dataType = 1;
}
void MapUnit_Move::DrawSimple(int y) {
	DrawGraph(1516, y, gr_Move, true);
	DrawString(1532, y + 8, "移動", BLACK);
}
//【動かない】
MapUnit_Move_None::MapUnit_Move_None() {
	detailsType = 0;
	change_detailsType = detailsType;
}
MapUnit_Move_None::~MapUnit_Move_None() {
	// 特になし
}
void MapUnit_Move_None::DrawSimple(int y) {
	DrawGraph(1516, y, gr_Move, true);
	DrawString(1532, y + 8, "移動なし", BLUE);
}
std::string MapUnit_Move_None::GetCsv() {
	string csv = "1,";	// データの種類

	csv += "0,\n";		// 挙動の種類
	return csv;
}
//【プレイヤーを追う or 逃げる】
MapUnit_Move_chase::MapUnit_Move_chase() {
	speed = 5;
	delay = 60;
	chaseFlag = true;

	detailsType = 1;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Move_chase::MapUnit_Move_chase(string line) {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	dataType = boost::lexical_cast<int>(L_token);
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【移動速度】取得
	std::getline(L_stream, L_token, ',');
	speed = boost::lexical_cast<int>(L_token);
	// 4【移動ごとのdelay】取得
	std::getline(L_stream, L_token, ',');
	delay = boost::lexical_cast<int>(L_token);
	// 【追うor逃げる】取得
	std::getline(L_stream, L_token, ',');
	chaseFlag = L_token == "1";

	SetPanel();
}
MapUnit_Move_chase::~MapUnit_Move_chase() {

}
void MapUnit_Move_chase::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 800, 300, "移動速度", &speed, "移動速度を入力してください\n\n\n\n\n\n速度0以下は不可、速度は64以上にはならない"));
	panel.push_back(new Button_InputNum(800, 200, 1000, 300, "移動delay", &delay, "移動ごとのdelayを入力してください"));
	panel.push_back(new FlagButton(1000, 200, 1200, 300, "追うor逃げる", &chaseFlag, "追う", "逃げる"));
}
void MapUnit_Move_chase::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawString(1532, y + 8, "プレイヤー", BLUE);
	DrawString(1632, y + 8, (chaseFlag ? "を追う" : "から逃げる"), BLUE);
}
std::string MapUnit_Move_chase::GetCsv() {
	string csv = "1,";	// データの種類

	csv += "1,";		// 挙動の種類
	csv += to_string(speed) + ',';
	csv += to_string(delay) + ',';
	csv += (chaseFlag ? "1" : "0");	// 
	csv += '\n';
	return csv;
}
//【ループ移動】
MapUnit_Move_Loop::MapUnit_Move_Loop() {
	speed = 5;
	delay = 60;
	moveData.push_back({ 1, 1 });

	detailsType = 2;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Move_Loop::MapUnit_Move_Loop(string line) {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	dataType = boost::lexical_cast<int>(L_token);
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【移動速度】取得
	std::getline(L_stream, L_token, ',');
	speed = boost::lexical_cast<int>(L_token);
	// 4【移動ごとのdelay】取得
	std::getline(L_stream, L_token, ',');
	delay = boost::lexical_cast<int>(L_token);
	// 【移動データ】
	while (std::getline(L_stream, L_token, ',')) {
		if (L_token == "") break;
		moveData.push_back({ boost::lexical_cast<int>(L_token), 1 });
		std::getline(L_stream, L_token, ',');
		moveData.back().moveNum = boost::lexical_cast<int>(L_token);
	}

	SetPanel();
}
MapUnit_Move_Loop::~MapUnit_Move_Loop() {

}
void MapUnit_Move_Loop::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 800, 300, "移動速度", &speed, "移動速度を入力してください\n\n\n\n\n\n速度0以下は不可、速度は64以上にはならない"));
	panel.push_back(new Button_InputNum(800, 200, 1000, 300, "移動delay", &delay, "移動ごとのdelayを入力してください"));
	panel.push_back(new Panel(1000, 200, 1300, 250, "移動データ"));
	panel.push_back(new Panel(1000, 250, 1100, 300, "移動方向"));
	panel.push_back(new Panel(1100, 250, 1300, 300, "移動数"));
	std::vector<std::string> tmp_menu = {
		"　↑　",
		"　↓　",
		"　←　",
		"　→　",
	};
	for (int i = 0, n = (int)moveData.size(); i < n; ++i) {		
		movePanel.push_back(new PulldownMenu_int(1000, 300 + i * 48, 1100, 300 + (i + 1) * 48, "", &(moveData[i].dir), tmp_menu));
		movePanel.push_back(new Button_InputNum(1100, 300 + i * 48, 1300, 300 + (i + 1) * 48, "", &(moveData[i].moveNum), "移動数を入力してください"));
	}
}
bool MapUnit_Move_Loop::Editing() {
	bool endFlag = true;

	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		if (!panel[i]->UpDate()) {
			endFlag = false;
		}
	}
	for (int i = 0, n = (int)movePanel.size(); i < n; ++i) {
		if (!movePanel[i]->UpDate()) {
			endFlag = false;
		}
	}

	if (MouseData::Get(MOUSE_INPUT_LEFT) == 1 && MouseData::GetX() > 1300 && MouseData::GetX() <= 1396 && MouseData::GetY() >= 300 && MouseData::GetY() <= 300 + (moveData.size() * 48)) {
		int tmp_selectN = (MouseData::GetY() - 300) / 48;
		//【移動データ追加】
		if (MouseData::GetX() < 1348) {
			moveData.insert(moveData.begin() + tmp_selectN + 1, { 0, 1 });
			for (int i = 0, n = (int)movePanel.size(); i < n; ++i) {
				delete movePanel[i];
			}
			movePanel.clear();
			movePanel.shrink_to_fit();
			std::vector<std::string> tmp_menu = {
				"　↑　",
				"　↓　",
				"　←　",
				"　→　",
			};
			for (int i = 0, n = (int)moveData.size(); i < n; ++i) {
				movePanel.push_back(new PulldownMenu_int(1000, 300 + i * 48, 1100, 300 + (i + 1) * 48, "", &(moveData[i].dir), tmp_menu));
				movePanel.push_back(new Button_InputNum(1100, 300 + i * 48, 1300, 300 + (i + 1) * 48, "", &(moveData[i].moveNum), "移動数を入力してください"));
			}
		}
		//【移動データ削除】
		else {
			// サイズが0にならないように
			if (moveData.size() > 1) {
				moveData.erase(moveData.begin() + tmp_selectN);
			}
			for (int i = 0, n = (int)movePanel.size(); i < n; ++i) {
				delete movePanel[i];
			}
			movePanel.clear();
			movePanel.shrink_to_fit();
			std::vector<std::string> tmp_menu = {
				"　↑　",
				"　↓　",
				"　←　",
				"　→　",
			};
			for (int i = 0, n = (int)moveData.size(); i < n; ++i) {
				movePanel.push_back(new PulldownMenu_int(1000, 300 + i * 48, 1100, 300 + (i + 1) * 48, "", &(moveData[i].dir), tmp_menu));
				movePanel.push_back(new Button_InputNum(1100, 300 + i * 48, 1300, 300 + (i + 1) * 48, "", &(moveData[i].moveNum), "移動数を入力してください"));
			}
		}
		endFlag = false;
	}


	if (change_detailsType != detailsType) {
		MapMaker::changeUnitDataWindow.flag = true;
		MapMaker::changeUnitDataWindow.selectTypeList = change_detailsType;
		MapMaker::changeUnitDataWindow.unitDataType = (eUnitData)dataType;
		change_detailsType = detailsType;
	}

	return endFlag;
}
void MapUnit_Move_Loop::DrawData() {
	for (int i = 0, n = (int)panel.size(); i < n; ++i) {
		panel[i]->Draw();
	}
	for (int i = 0, n = (int)movePanel.size(); i < n; ++i) {
		movePanel[i]->Draw();
		if (i & 1) {
			DrawGraph(1300, 300 + (i / 2) * 48, gr_Add_Des, false);
		}
	}
}
void MapUnit_Move_Loop::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawString(1532, y + 8, "ループ移動", BLUE);
}
std::string MapUnit_Move_Loop::GetCsv() {
	string csv = "1,";	// データの種類

	csv += "2,";		// 挙動の種類
	csv += to_string(speed) + ',';
	csv += to_string(delay) + ',';
	for (int i = 0, n = (int)moveData.size(); i < n; ++i) {
		csv += to_string(moveData[i].dir) + ',';
		csv += to_string(moveData[i].moveNum) + ',';
	}
	csv += '\n';
	return csv;
}
//【ランダム】
MapUnit_Move_Random::MapUnit_Move_Random() {
	speed = 5;
	delay = 60;

	detailsType = 3;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Move_Random::MapUnit_Move_Random(string line) {
	//【配置データ読み込み】
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】破棄
	std::getline(L_stream, L_token, ',');
	dataType = boost::lexical_cast<int>(L_token);
	// 2【挙動のタイプ】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 3【移動速度】取得
	std::getline(L_stream, L_token, ',');
	speed = boost::lexical_cast<int>(L_token);
	// 4【移動ごとのdelay】取得
	std::getline(L_stream, L_token, ',');
	delay = boost::lexical_cast<int>(L_token);

	SetPanel();
}
MapUnit_Move_Random::~MapUnit_Move_Random() {

}
void MapUnit_Move_Random::SetPanel() {
	panel.push_back(new Button_InputNum(600, 200, 800, 300, "移動速度", &speed, "移動速度を入力してください\n\n\n\n\n\n速度0以下は不可、速度は64以上にはならない"));
	panel.push_back(new Button_InputNum(800, 200, 1000, 300, "移動delay", &delay, "移動ごとのdelayを入力してください"));
}
void MapUnit_Move_Random::DrawSimple(int y) {
	DrawGraph(1516, y, gr_If, true);
	DrawString(1532, y + 8, "ランダム移動", BLUE);
}
std::string MapUnit_Move_Random::GetCsv() {
	string csv = "1,";	// データの種類

	csv += "3,";		// 挙動の種類
	csv += to_string(speed) + ',';
	csv += to_string(delay) ;
	csv += '\n';
	return csv;
}
/*-------------------------------------------------ユニットのモード--------------------------------------------*/
MapUnit_Mode::MapUnit_Mode() : UnitData() {
	std::vector<std::string> tmp_menu = {
		"条件なし",
		"ストーリー進行度",
		"イベントフラグ"
	};
	panel.push_back(new PulldownMenu_int(300, 200, 600, 300, "モードの種類", &change_detailsType, tmp_menu));
	panel.push_back(new FlagButton(600, 200, 800, 300, "当たり判定", &collisionFlag, "あり", "なし"));
	panel.push_back(new FlagButton(800, 200, 1000, 300, "出現フラグ", &appearFlag, "出現", "消滅"));

	dataType = 0;
}

//【条件なし】
MapUnit_Mode_None::MapUnit_Mode_None() {
	modeNum = 0;
	collisionFlag = true;
	appearFlag = true;

	detailsType = 0;
	change_detailsType = detailsType;
	SetPanel();
}
MapUnit_Mode_None::MapUnit_Mode_None(string line) {
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】
	std::getline(L_stream, L_token, ',');
	dataType = boost::lexical_cast<int>(L_token);
	// 【モード数】
	std::getline(L_stream, L_token, ',');
	modeNum = boost::lexical_cast<int>(L_token);
	// 【当たり判定】
	std::getline(L_stream, L_token, ',');
	collisionFlag = L_token == "1";
	// 【出現フラグ】
	std::getline(L_stream, L_token, ',');
	appearFlag = L_token == "1";
	// 【移行条件】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
}
MapUnit_Mode_None::~MapUnit_Mode_None() {

}
void MapUnit_Mode_None::DrawSimple(int y) {
	DrawGraph(1500, y, gr_Mode, true);
	DrawFormatString(1516, y + 8, BLACK, "モード%d", modeNum);
	DrawString(1600, y + 8, "条件なし", BLACK);
}
std::string MapUnit_Mode_None::GetCsv() {
	string csv = to_string(dataType) + ",";	// データの種類
	csv += to_string(modeNum) + ',';		// モード数
	csv += (collisionFlag ? "1," : "0,");	// 当たり判定
	csv += (appearFlag ? "1," : "0,");		// 出現フラグ
	csv += to_string(detailsType);			// 条件のタイプ
	csv += '\n';
	return csv;
}
//【ストーリー進行度】
MapUnit_Mode_Chapter::MapUnit_Mode_Chapter() {
	modeNum = 0;
	chapter = 0;
	term = 0;

	// 編集画面用
	termSelectFlag = false;
}
MapUnit_Mode_Chapter::MapUnit_Mode_Chapter(string line) {
	//ロード用変数
	std::istringstream L_stream(line);
	string L_token;

	// 1【データの種類】
	std::getline(L_stream, L_token, ',');
	dataType = boost::lexical_cast<int>(L_token);
	// 【モード数】
	std::getline(L_stream, L_token, ',');
	modeNum = boost::lexical_cast<int>(L_token);
	// 【当たり判定】
	std::getline(L_stream, L_token, ',');
	collisionFlag = L_token == "1";
	// 【出現フラグ】
	std::getline(L_stream, L_token, ',');
	appearFlag = L_token == "1";
	// 【移行条件】
	std::getline(L_stream, L_token, ',');
	detailsType = boost::lexical_cast<int>(L_token);
	change_detailsType = boost::lexical_cast<int>(L_token);
	// 【基準の進行度】
	std::getline(L_stream, L_token, ',');
	chapter = boost::lexical_cast<int>(L_token);
	// 【条件】
	std::getline(L_stream, L_token, ',');
	term = boost::lexical_cast<int>(L_token);

	// 編集画面用
	termSelectFlag = false;
}
MapUnit_Mode_Chapter::~MapUnit_Mode_Chapter() {

}
bool MapUnit_Mode_Chapter::Editing() {
	// 条件の種類変更
	if (ChangeTypeFlag) {
		// スクロール
		scroll -= MouseData::GetWheel() * 50;
		// 左クリックで決定
		if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
			if (MouseData::GetX() > 502 && MouseData::GetX() < 700) {
				int selectN = (MouseData::GetY() + scroll - 150) / 64;
				if (selectN >= 0 && selectN < 2) {
					MapMaker::changeUnitDataWindow.flag = true;
					MapMaker::changeUnitDataWindow.selectTypeList = selectN;
					MapMaker::changeUnitDataWindow.unitDataType = eUnitData::Mode;
				}
			}
			ChangeTypeFlag = false;
		}
	}
	// 左クリックで編集
	else if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
		//【条件】の選択
		if (termSelectFlag) {
			if (MouseData::GetX() > 1084 && MouseData::GetX() < 1275) {
				int selectN = (MouseData::GetY() - 278) / 64;
				if (selectN >= 0 && selectN < 4) {
					term = selectN;
				}
			}
			termSelectFlag = false;	// クリックで終了
		}
		else if (MouseData::GetX() > 502 && MouseData::GetX() < 1275 &&
			MouseData::GetY() > 140 && MouseData::GetY() < 288) {
			//【モードの種類】
			if (MouseData::GetX() < 700) {
				ChangeTypeFlag = true;
				scroll = 0;
			}
			//【モード番号】
			else if(MouseData::GetX() < 892){
				MapMaker::inputData = new InputNum(&modeNum, "モード番号を入力してください");
			}
			//【基準の進行度】
			else if (MouseData::GetX() < 1084) {
				MapMaker::inputData = new InputNum(&chapter, "基準の進行度を入力してください");
			}
			//【条件】
			else {
				termSelectFlag = true;
			}
		}
		else {
			return true;	// 画面外クリックで終了
		}
	}

	return false;
}
void MapUnit_Mode_Chapter::DrawData() {
	// 枠
	DrawGraph(492, 140, gr_Editing_Mode_Chapter, true);
	DrawFormatStringFToHandle(710, 230, BLACK, Font::Get(eFont::UNIT), "%d", modeNum);	// モード番号
	DrawFormatStringFToHandle(912, 230, BLACK, Font::Get(eFont::UNIT), "%d", chapter);	// 基準の進行度
	// 条件
	switch (term) {
	case 0://【nのとき】
		DrawStringToHandle(1094, 230, "のとき", BLACK, Font::Get(eFont::UNIT2));
		break;
	case 1://【n以外のとき】
		DrawStringToHandle(1094, 230, "以外のとき", BLACK, Font::Get(eFont::UNIT2));
		break;
	case 2://【n以前】
		DrawStringToHandle(1094, 230, "以前", BLACK, Font::Get(eFont::UNIT2));
		break;
	case 3://【n以降】
		DrawStringToHandle(1094, 230, "以降", BLACK, Font::Get(eFont::UNIT2));
		break;
	default:
		break;
	}
	// 対象選択のプルダウンメニュー
	if (termSelectFlag) {
		// 挙動【ダメージ】のものを再利用
		DrawGraph(1084, 278, gr_Editing_Mode_Chapter2, true);
	}
	// モードの種類変更
	if (ChangeTypeFlag) {
		DrawGraph(502, 150 - scroll, gr_ModeList, true);
	}
}
void MapUnit_Mode_Chapter::DrawSimple(int y) {
	DrawGraph(1500, y, gr_Mode, true);
	DrawFormatString(1516, y + 8, BLACK, "モード%d", modeNum);
	DrawFormatString(1600, y + 8, BLACK, "ストーリ進行度%d", chapter);
	switch (term){
	case 0://【nのとき】
		DrawString(1750, y + 8, "のとき", BLACK);
		break;
	case 1://【n以外のとき】
		DrawString(1750, y + 8, "以外のとき", BLACK);
		break;
	case 2://【n以前】
		DrawString(1750, y + 8, "以前", BLACK);
		break;
	case 3://【n以降】
		DrawString(1750, y + 8, "以降", BLACK);
		break;
	default:
		break;
	}
}
std::string MapUnit_Mode_Chapter::GetCsv() {
	string csv = "0,";	// データの種類
	csv += to_string(modeNum) + ',';	// モード数

	csv += "1,";						// 挙動の種類
	csv += to_string(chapter) + ',';	// 基準の進行度
	csv += to_string(term) + ',';		// 条件
	csv += '\n';
	return csv;
}
/*-----------------------------------------ユニット詳細データの追加画面-----------------------------------------*/

// ユニットデータのファクトリ
UnitData* Factory_UnitData(string line) {
	string token;

	// 読み込んだ行をコピー
	std::istringstream stream(line);
	// 1列目をロードして判定
	std::getline(stream, token, ',');

	switch (boost::lexical_cast<int>(token)){
	case 0:	//【モード】
		// 2【モード数】破棄
		std::getline(stream, token, ',');
		// 【当たり判定】破棄
		std::getline(stream, token, ',');
		// 【出現フラグ】破棄
		std::getline(stream, token, ',');
		// 3【モードタイプ】判定
		std::getline(stream, token, ',');
		switch (boost::lexical_cast<int>(token)){
		case 0://【条件なし】
			return new MapUnit_Mode_None(line);
			break;
		case 1://【イベント進行度】
			return new MapUnit_Mode_Chapter(line);
			break;
		default:
			break;
		}
		break;
	case 1:	//【移動】
		// 2【移動タイプ】判定
		std::getline(stream, token, ',');
		switch (boost::lexical_cast<int>(token))
		{
		case 0:	//【動かない】
			return new MapUnit_Move_None();
			break;
		case 1:	//【主人公を追うor逃げる】
			return new MapUnit_Move_chase(line);
			break;
		case 2:	//【ループ移動】
			return new MapUnit_Move_Loop(line);
			break;
		case 3:	//【ランダム移動】
			return new MapUnit_Move_Random(line);
			break;
		}
		break;
	case 2:	//【挙動の実行条件】
		// 2【条件タイプ】判定
		std::getline(stream, token, ',');
		switch (boost::lexical_cast<int>(token)){
		case 0:	//【話しかける】
			return new MapUnit_If_Talk();
			break;
		case 1:	//【触れる】
			return new MapUnit_If_Touch();
			break;
		case 2:	//【メッセージを受信する】
			return new MapUnit_If_Message(line);
			break;
		case 3:	//【常時】
			return new MapUnit_If_Always();
			break;
		case 4:	//【主人公との距離判定】
			return new MapUnit_If_Distance(line);
			break;
		}
		break;
	case 3:	//【挙動】
		// 2【挙動タイプ】判定
		std::getline(stream, token, ',');
		switch (boost::lexical_cast<int>(token))
		{
		case 0:	//【転移】
			return new MapUnit_Action_Warp(line);
			break;
		case 1:	//【セリフ】
			return new MapUnit_Action_Talk(line);
			break;
		case 2:	//【戦闘】
			return new MapUnit_Action_Battle(line);
			break;
		case 3:	//【ダメージ】
			return new MapUnit_Action_Damage(line);
			break;
		case 4:	//【回復】
			return new MapUnit_Action_Recovery(line);
			break;
		case 5:	//【アイテム取得】
			return new MapUnit_Action_Item(line);
			break;
		case 6:	//【店】
			return new MapUnit_Action_Shop(line);
			break;
		case 7:	//【消滅-出現】
			//return new MapUnit_Action_Existence(line);
			break;
		case 8:	//【モード変更】
			return new MapUnit_Action_ChangeMode(line);
			break;
		case 9:	//【メッセージ】
			return new MapUnit_Action_Message(line);
			break;
		case 10: //【選択分岐】
			return new MapUnit_Action_SelectBranch(line);
			break;
		case 11: //【強制終了】
			return new MapUnit_Action_End();
			break;
		case 12://【挙動番号の移動】
			return new MapUnit_Action_Goto(line);
			break;
		case 13://【強制ゲームオーバー】
			return new MapUnit_Action_GameOver();
			break;
		case 14://【ゲームクリア】
			return new MapUnit_Action_GameClear();
			break;
		case 15:// 【金を払う】
			return new MapUnit_Action_PayMoney(line);
			break;
		default:
			break;
		}
		break;
	}
	return new UnitData_Default(line);
}

UnitData* Factory_UnitData(int dataType, int detailsType) {
	switch (dataType) {
	case 0:	//【モード】
		switch (detailsType) {
		case 0://【条件なし】
			return new MapUnit_Mode_None();
			break;
		case 1://【イベント進行度】
			return new MapUnit_Mode_Chapter();
			break;
		default:
			break;
		}
		break;
	case 1:	//【移動】
		switch (detailsType) {
		case 0:	//【動かない】
			return new MapUnit_Move_None();
			break;
		case 1:	//【主人公を追うor逃げる】
			return new MapUnit_Move_chase();
			break;
		case 2:	//【ループ移動】
			return new MapUnit_Move_Loop();
			break;
		case 3:	//【ランダム移動】
			return new MapUnit_Move_Random();
			break;
		}
		break;
	case 2:	//【挙動の実行条件】
		switch (detailsType) {
		case 0:	//【話しかける】
			return new MapUnit_If_Talk();
			break;
		case 1:	//【触れる】
			return new MapUnit_If_Touch();
			break;
		case 2:	//【メッセージを受信する】
			return new MapUnit_If_Message();
			break;
		case 3:	//【常時】
			return new MapUnit_If_Always();
			break;
		case 4:	//【主人公との距離判定】
			return new MapUnit_If_Distance();
			break;
		}
		break;
	case 3:	//【挙動】
			// 2【挙動タイプ】判定
		switch (detailsType) {
		case 0:	//【転移】
			return new MapUnit_Action_Warp();
			break;
		case 1:	//【セリフ】
			return new MapUnit_Action_Talk();
			break;
		case 2:	//【戦闘】
			return new MapUnit_Action_Battle();
			break;
		case 3:	//【ダメージ】
			return new MapUnit_Action_Damage();
			break;
		case 4:	//【回復】
			return new MapUnit_Action_Recovery();
			break;
		case 5:	//【アイテム取得】
			return new MapUnit_Action_Item();
			break;
		case 6:	//【店】
			return new MapUnit_Action_Shop();
			break;
		case 7:	//【消滅-出現】
				//return new MapUnit_Action_Existence();
			break;
		case 8:	//【モード変更】
			return new MapUnit_Action_ChangeMode();
			break;
		case 9:	//【メッセージ】
			return new MapUnit_Action_Message();
			break;
		case 10: //【選択分岐】
			return new MapUnit_Action_SelectBranch();
			break;
		case 11: //【強制終了】
			return new MapUnit_Action_End();
			break;
		case 12://【挙動番号の移動】
			return new MapUnit_Action_Goto();
			break;
		case 13://【強制ゲームオーバー】
			return new MapUnit_Action_GameOver();
			break;
		case 14://【ゲームクリア】
			return new MapUnit_Action_GameClear();
			break;
		case 15:// 【金を払う】
			return new MapUnit_Action_PayMoney();
			break;
		default:
			break;
		}
		break;
	}
	return new MapUnit_Action_Warp();
}

//【ユニット詳細データの追加画面】
int AddUnitData::gr_back;			// 背景,枠
int AddUnitData::gr_UnitDataList;	// データの種類のリスト
int AddUnitData::gr_ModeList;		// モードリスト
int AddUnitData::gr_MoveList;		// 移動リスト
int AddUnitData::gr_IfList;			// 挙動の実行条件リスト
int AddUnitData::gr_ActList;		// 挙動リスト

AddUnitData::AddUnitData(std::vector<UnitData*>& unitData, int insertPos):
	unitData(unitData),
	insertPos(insertPos)
{
	unitDataType = eUnitData::Mode;
	selectTypeList = 0;
	selectFlag_UnitData = false;
	selectFlag_TypeList = false;
	scroll = 0;
}

AddUnitData::~AddUnitData() {

}

void AddUnitData::Load(){
	gr_back = LoadGraph("img\\mapCreater\\AddUnitData.png");			// 背景,枠
	gr_UnitDataList = LoadGraph("img\\mapCreater\\UnitDataList.png");	// データの種類のリスト
	gr_ModeList = LoadGraph("img\\mapCreater\\ModeList.png");			// モードリスト
	gr_MoveList = LoadGraph("img\\mapCreater\\MoveList.png");			// 移動リスト
	gr_IfList = LoadGraph("img\\mapCreater\\IfList.png");				// 挙動の実行条件リスト
	gr_ActList = LoadGraph("img\\mapCreater\\ActList.png");				// 挙動リスト
}

void AddUnitData::Release() {
	DeleteGraph(gr_back);			// 背景,枠
	DeleteGraph(gr_UnitDataList);	// データの種類のリスト
	DeleteGraph(gr_ModeList);		// モードリスト
	DeleteGraph(gr_MoveList);		// 移動リスト
	DeleteGraph(gr_IfList);			// 挙動の実行条件リスト
	DeleteGraph(gr_ActList);		// 挙動リスト
}

bool AddUnitData::UpDate() {
	//【決定】
	if (KeyData::Get(KEY_INPUT_RETURN) == 1) {
		unitData.insert(unitData.begin() + insertPos, Factory_UnitData((int)unitDataType, selectTypeList));
		return true;
	}
	//【取り消し】
	if (KeyData::Get(KEY_INPUT_ESCAPE) == 1) {
		return true;
	}

	// スクロール
	if (selectFlag_UnitData || selectFlag_TypeList) {
		scroll -= MouseData::GetWheel() * 50;
	}

	// 左クリックで編集
	if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
		//【データの種類】
		if (selectFlag_UnitData) {
			if (MouseData::GetX() > 700 && MouseData::GetX() < 892) {
				int selectN = (MouseData::GetY() - scroll - 278) / 64;
				if (selectN >= 0 && selectN < 4) {
					unitDataType = (eUnitData)selectN;
					selectTypeList = 0;
				}
			}
			selectFlag_UnitData = false;	// クリックで終了
		}
		//【種類ごとのタイプ選択】
		else if (selectFlag_TypeList) {
			switch (unitDataType){
			case eUnitData::Mode://【モード】
				if (MouseData::GetX() > 892 && MouseData::GetX() < 1082) {
					int selectN = (MouseData::GetY() + scroll - 278) / 64;
					if (selectN >= 0 && selectN < 2) {
						selectTypeList = selectN;
					}
				}
				break;
			case eUnitData::Move://【移動】
				if (MouseData::GetX() > 892 && MouseData::GetX() < 1082) {
					int selectN = (MouseData::GetY() + scroll - 278) / 64;
					if (selectN >= 0 && selectN < 4) {
						if (selectN == 1) break;	// 未実装（追うor逃げる）
						if (selectN == 2) break;	// 未実装（ループ移動）
						selectTypeList = selectN;
					}
				}
				break;
			case eUnitData::If://【挙動の実行条件】
				if (MouseData::GetX() > 892 && MouseData::GetX() < 1082) {
					int selectN = (MouseData::GetY() + scroll - 278) / 64;
					if (selectN >= 0 && selectN < 5) {
						selectTypeList = selectN;
					}
				}
				break;
			case eUnitData::Act://【挙動】
				if (MouseData::GetX() > 892 && MouseData::GetX() < 1082) {
					int selectN = (MouseData::GetY() + scroll - 278) / 64;
					if (selectN >= 0 && selectN < 15) {
						selectTypeList = selectN;
					}
				}
				break;
			default:
				break;
			}
			selectFlag_TypeList = false;	// クリックで終了
		}
		else if (MouseData::GetX() > 700 && MouseData::GetX() < 1082 &&
			MouseData::GetY() > 150 && MouseData::GetY() < 278) {
			//【データの種類】
			if (MouseData::GetX() < 892) {
				selectFlag_UnitData = true;
				scroll = 0;
			}
			//【種類ごとのタイプ選択】
			else {
				selectFlag_TypeList = true;
				scroll = 0;
			}
		}
	}
	return false;
}

void AddUnitData::Draw() {
	// 背景,枠
	DrawGraph(690, 108, gr_back, true);

	// データの種類
	switch (unitDataType) {
	case eUnitData::Mode://【モード】
	{
		DrawStringToHandle(710, 230, "モード", BLACK, Font::Get(eFont::UNIT2));
		DrawStringToHandle(902, 170, "モードの種類", BLUE, Font::Get(eFont::UNIT2));
		switch (selectTypeList) {
		case 0://【条件なし】
			DrawStringToHandle(902, 230, "条件なし", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 1://【ストーリー進行度】
			DrawStringToHandle(902, 230, "ｽﾄｰﾘｰ進行度", BLACK, Font::Get(eFont::UNIT2));
			break;
		default:
			break;
		}
		break;
	}
	case eUnitData::Move://【移動】
	{
		DrawStringToHandle(710, 230, "移動", BLACK, Font::Get(eFont::UNIT2));
		DrawStringToHandle(902, 170, "移動の種類", BLUE, Font::Get(eFont::UNIT2));
		switch (selectTypeList){
		case 0:	//【動かない】
			DrawStringToHandle(902, 230, "移動なし", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 1:	//【主人公を追うor逃げる】
			DrawStringToHandle(902, 230, "追うor逃げる", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 2:	//【ループ移動】
			DrawStringToHandle(902, 230, "ループ移動", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 3:	//【ランダム移動】
			DrawStringToHandle(902, 230, "ランダム移動", BLACK, Font::Get(eFont::UNIT2));
			break;
		}
		break;
	}
	case eUnitData::If://【挙動の実行条件】
	{
		DrawStringToHandle(710, 230, "挙動の実行条件", BLACK, Font::Get(eFont::ID));
		DrawStringToHandle(902, 170, "条件の種類", BLUE, Font::Get(eFont::UNIT2));
		switch (selectTypeList){
		case 0:	//【話しかける】
			DrawStringToHandle(902, 230, "話しかける", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 1:	//【触れる】
			DrawStringToHandle(902, 230, "触れる", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 2:	//【メッセージを受信する】
			DrawStringToHandle(902, 230, "メッセージ", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 3:	//【常時】
			DrawStringToHandle(902, 230, "常時", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 4:	//【主人公との距離判定】
			DrawStringToHandle(902, 230, "近づく", BLACK, Font::Get(eFont::UNIT2));
			break;
		}
		break;
	}
	case eUnitData::Act://【挙動】
	{
		DrawStringToHandle(710, 230, "挙動", BLACK, Font::Get(eFont::UNIT2));
		DrawStringToHandle(902, 170, "挙動の種類", BLUE, Font::Get(eFont::UNIT2));
		const int x = 902;
		const int y = 230;
		switch (selectTypeList){
		case 0:	//【転移】
			DrawStringToHandle(x, y, "転移", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 1:	//【セリフ】
			DrawStringToHandle(x, y, "セリフ", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 2:	//【戦闘】
			DrawStringToHandle(x, y, "戦闘", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 3:	//【ダメージ】
			DrawStringToHandle(x, y, "ダメージ", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 4:	//【回復】
			DrawStringToHandle(x, y, "回復", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 5:	//【アイテム取得】
			DrawStringToHandle(x, y, "アイテム取得", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 6:	//【店】
			DrawStringToHandle(x, y, "店", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 7:	//【消滅-出現】
			DrawStringToHandle(x, y, "消滅-出現", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 8:	//【モード変更】
			DrawStringToHandle(x, y, "モード変更", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 9:	//【メッセージ】
			DrawStringToHandle(x, y, "メッセージ", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 10: //【選択分岐】
			DrawStringToHandle(x, y, "選択分岐", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 11: //【強制終了】
			DrawStringToHandle(x, y, "強制終了", BLACK, Font::Get(eFont::UNIT2));
			break;
		case 12://【挙動番号の移動】
			DrawStringToHandle(x, y, "挙動のジャンプ", BLACK, Font::Get(eFont::ID));
			break;
		case 13://【強制ゲームオーバー】
			DrawStringToHandle(x, y, "ゲームオーバー", BLACK, Font::Get(eFont::ID));
			break;
		case 14://【ゲームクリア】
			DrawStringToHandle(x, y, "ゲームクリア", BLACK, Font::Get(eFont::ID));
			break;
		}
		break;
	}
	default:
		break;
	}

	//【データの種類のリスト】
	if (selectFlag_UnitData) {
		DrawGraph(700, 278 - scroll, gr_UnitDataList, true);
	}
	//【種類ごとのタイプ選択リスト】
	if (selectFlag_TypeList) {
		switch (unitDataType){
		case eUnitData::Mode:
			DrawGraph(892, 278 - scroll, gr_ModeList, true);
			break;
		case eUnitData::Move:
			DrawGraph(892, 278 - scroll, gr_MoveList, true);
			break;
		case eUnitData::If:
			DrawGraph(892, 278 - scroll, gr_IfList, true);
			break;
		case eUnitData::Act:
			DrawGraph(892, 278 - scroll, gr_ActList, true);
			break;
		default:
			break;
		}
	}
}