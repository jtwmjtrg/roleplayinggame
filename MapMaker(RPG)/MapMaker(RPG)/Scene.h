#pragma once

// シーンの列挙
enum class eScene : __int8 {
	S_DataSelect,// データ選択
	S_MapMaker,	// マップ作成
	S_PutUnit,	// ユニット配置
	S_End
};

class SuperScene {
protected:
	bool endFlag;	// シーン終了フラグ trueで終了

	eScene nextScene;	// 次のシーン【受け渡す用】

public:
	SuperScene();
	~SuperScene();

	virtual void UpDate() = 0;	// アップデート

	virtual void Draw() = 0;// 描画

	bool GetEndFlag();
	eScene GetNextScene();
};