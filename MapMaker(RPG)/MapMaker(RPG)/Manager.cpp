#include "Manager.h"

Manager::Manager() {
	this->endFlag = false;
	this->NowScene = eScene::S_DataSelect;

	this->dataSelect = nullptr;
	this->mapMaker = nullptr;


	this->dataSelect = new DataSelect();
}
Manager::~Manager() {
	delete dataSelect;
	delete mapMaker;
}

void Manager::UpDate() {

	// 【画面切り替え】
	switch (this->NowScene) {
	case eScene::S_DataSelect://選択画面
		// エンドフラグが立ったら次のシーンへ移行
		if (this->dataSelect->GetEndFlag()) {
			InitGraph();	// 全グラフィック削除
			InitSoundMem();	// 曲データ全削除
			this->NowScene = this->dataSelect->GetNextScene();
			
			this->mapMaker = new MapMaker(this->dataSelect->GetDataPath());
			delete this->dataSelect;
			this->dataSelect = nullptr;
		}
		break;
	case eScene::S_MapMaker:// マップ作製
		if (this->mapMaker->GetEndFlag()) {
			InitGraph();	// 全グラフィック削除
			InitSoundMem();	// 曲データ全削除
			this->NowScene = this->mapMaker->GetNextScene();

			delete  this->mapMaker;
			this->mapMaker = nullptr;
			this->dataSelect = new DataSelect();
		}
		break;
	}


	// 【各画面アップデート】
	switch (this->NowScene) {
	case eScene::S_DataSelect://選択画面
		this->dataSelect->UpDate();
		break;
	case eScene::S_MapMaker://マップ作製
		this->mapMaker->UpDate();
		break;
	default:	//Error
		this->endFlag = true;
		break;
	}
	
}

void Manager::Draw() {
	// 【各画面描画】
	switch (this->NowScene) {
	case eScene::S_DataSelect://選択画面
		this->dataSelect->Draw();
		break;
	case eScene::S_MapMaker://マップ作製
		this->mapMaker->Draw();
		break;
	default:	//Error
		this->endFlag = true;
		break;
	}
}