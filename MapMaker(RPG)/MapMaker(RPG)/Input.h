#pragma once
#include "DxLib.h"
#include <iostream>
#include <string>
#include "Font.h"

using namespace std;

// キーボードの入力状態
class KeyData {
private:
	static int key[256];	// キーの入力状態格納用変数
	static char tmpKey[256];	// 現在のキーの入力状態を格納する

public:
	KeyData();
	~KeyData();

	static void UpDate();	// キー入力の状態更新
	static int Get(int KeyCode);	// キー入力状態取得
	static bool CheckEnd();	// 強制終了
};

// ゲームパッドの入力状態
class PadData {
private:
	static int button[4][16];  // ゲームパッドの入力状態格納用変数
	static XINPUT_STATE input[4];	// ゲームパッドのナンバー
	static __int8 padNum;	// 繋がってるゲームパッドの数

public:
	PadData();
	~PadData();
	
	static void SetPadNum();
	static void UpDate();	// ゲームパッドの入力の状態更新
	static int Get(int code, int padNum);	// ゲームパッドの入力状態取得
	static bool CheckEnd();	// 強制終了
};

// マウスの入力状態
class MouseData {
private:
	static int x, y;		// 座標
	static int pre_x, pre_y;// 直前の座標
	static int wheel;		// ホイールの回転量
	static int mouse[3];	// マウスの入力状態格納用変数
	static int mouseInput;	// マウスの押した状態取得

public:
	MouseData();
	~MouseData();

	static void UpDate();	// マウスのクリックの状態を更新する
	static int Get(int MouseCode);	// マウスのクリックの状態を返す
	static int GetX();	// 座標取得
	static int GetY();
	static int GetWheel();	// ホイールの回転量取得
	static void PosCorrection();	// 直前座標に戻す
};


//【標準入力データ】
class InputData {
protected:
	std::string str;	// 上に表示する文字列
	int handle;			// 入力ハンドル
public:
	InputData(std::string str) : str(str) {}
	~InputData() { DeleteKeyInput(handle); }

	//virtual void Input() = 0;
	virtual void Draw() = 0;
	virtual bool UpDate() = 0;
};
// 文字列用
class InputStr : public InputData {
private:
	// 入力用データ
	std::string* destination;	// 入力先
public:
	InputStr(std::string* destination, std::string str);
	~InputStr() {}

	//void Input();
	bool UpDate();
	void Draw();
};
// 数値用
class InputNum : public InputData {
private:
	int* destination;	// 入力先
public:
	InputNum(int* destination, std::string str);
	~InputNum() {}

	//void Input();
	bool UpDate();
	void Draw();
};