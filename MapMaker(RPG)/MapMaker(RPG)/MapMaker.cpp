#include "MapMaker.h"

InputData* MapMaker::inputData;	// 標準入力データ
tChangeUnitDataWindow MapMaker::changeUnitDataWindow;	// ユニット詳細データの種類変更時の確認画面

MapMaker::MapMaker(std::string dataPath) {
	this->endFlag = false;
	this->nextScene = eScene::S_End;
	this->mode = eMakeType::Map;

	this->dataPath = dataPath;

	/*-----------------------------共通----------------------------------*/

	// 【画像読み込み】
	this->gr_mode[0] = LoadGraph("img\\mapCreater\\Mode_Map.png");		// モード切替
	this->gr_mode[1] = LoadGraph("img\\mapCreater\\Mode_PutUnit.png");
	this->gr_Back = LoadGraph("img\\mapCreater\\MapMaker_Back.png");	// 背景
	this->gr_Cursor = LoadGraph("img\\mapCreater\\cursor.png");	// カーソル
	this->gr_SelectChip = LoadGraph("img\\mapCreater\\SelectChip.png");	// 選択中のチップ
	
	// スクロール
	this->scrollX = - 500;
	this->scrollY = -500;

	chipSize = 64;		// チップのサイズ

	SaveSuccess = 0;

	/*-----------------------------マップ作製----------------------------------*/		
	// 画像
	this->gr_ChipList[0] = LoadGraph("img\\mapCreater\\ChipList_FLO.png");	// チップリスト背景
	this->gr_ChipList[1] = LoadGraph("img\\mapCreater\\ChipList_WAL.png");
	this->gr_AddLine = LoadGraph("img\\mapCreater\\+-.png");	// 行追加/削除の画像
	// マップチップ画像読み込み
	this->LoadMapChip();

	// チップリストのスクロール
	scroll_list_FLO = 0;	// 床
	scroll_list_WAL = 0;	// 壁

	// 選択中のマップ座標
	this->selectX = 0;
	this->selectY = 0;
	// 選択中のチップ
	this->selectChip.data = 0;
	this->selectChip.type = eChipType::FLO;
	this->selectChip.x = 0;
	this->selectChip.y = 0;

	// マップデータ読み込み
	this->LoadMapData();

	// パネルクラスの初期化
	Panel::Init();

	/*-----------------------------ユニット配置----------------------------------*/
	// 画像
	this->gr_UnitList[0] = LoadGraph("img\\mapCreater\\UnitList_NPC.png");	// チップリスト背景
	this->gr_UnitList[1] = LoadGraph("img\\mapCreater\\UnitList_OBJ.png");
	gr_PutUnitStatus = LoadGraph("img\\mapCreater\\PutUnitStatus.png");
	gr_UnitGrBack = LoadGraph("img\\mapCreater\\UnitGrBack.png");

	UnitData::Load();	// ユニットデータの画像読み込み

	this->LoadUnitGr();	// ユニットの画像ロード

	this->LoadPutUnit();// ユニット配置データロード

	selectUnit = nullptr;	// 選択したユニット
	unitMoveFlag = false;	// ユニット移動フラグ
	unitGrSelectFlag = false;	// ユニットの画像選択フラグ
	unitIdChangeFlag = false;	// ユニットIDの変更フラグ

	// 標準入力データ
	inputData = nullptr;

	/*-----------------------------ユニットデータ----------------------------------*/
	AddUnitData::Load();	// ユニット詳細データの追加用クラスの画像読み込み

	unitDataSelectFlag = false;	// ユニットデータの選択フラグ
	selectUnitDataNum = 0;		// 選択したユニットデータの番号
	scroll_UnitData = 0;		// ユニットデータのリストのスクロール

	// ユニット詳細データの削除時の確認画面
	deleteUnitDataWindow = { LoadGraph("img\\mapCreater\\DeleteUnitDataWindow.png"), false, -1 };

	// ユニット詳細データの種類変更時の確認画面
	changeUnitDataWindow = { LoadGraph("img\\mapCreater\\ChangeUnitDataWindow.png"), false , eUnitData::Act, -1};
	
	addUnitData = nullptr;	// ユニット詳細データの追加	
}

MapMaker::~MapMaker() {
	SaveMapData();	// マップデータのセーブ
	SavePutUnit();	// ユニットデータのセーブ

	// 画像データ
	// 左上のモード選択の画像
	for (int i = 0, n = (int)gr_mode.size(); i < n; ++i) {
		DeleteGraph(gr_mode[i]);
	}
	DeleteGraph(gr_Back);		// 背景
	DeleteGraph(gr_Cursor);		// カーソル
	DeleteGraph(gr_SelectChip);	// 選択中のチップ
	DeleteGraph(gr_AddLine);	// 行追加/削除の画像
	// マップチップの画像
	for (auto i = gr_mapChip.begin(), n = gr_mapChip.end(); i != n; ++i) {
		DeleteGraph(i->second);
	}
	gr_mapChip.clear();
	// チップリストの背景
	for (int i = 0, n = (int)gr_ChipList.size(); i < n; ++i) {
		DeleteGraph(gr_ChipList[i]);
	}
	// ユニットの画像
	for (auto i = gr_unit.begin(), n = gr_unit.end(); i != n; ++i) {
		for (int j = 0; j < 16; ++j) {
				DeleteGraph(i->second[j]);
		}
	}
	DeleteGraph(gr_PutUnitStatus);	// ユニット配置データの枠
	DeleteGraph(gr_UnitGrBack);		// 画像リストの枠
	// ユニットリストの背景
	for (int i = 0, n = (int)gr_UnitList.size(); i < n; ++i) {
		DeleteGraph(gr_UnitList[i]);
	}
	DeleteGraph(deleteUnitDataWindow.gr_Back);		// ユニット詳細データの削除時の確認画面
	DeleteGraph(changeUnitDataWindow.gr_Back);		// ユニット詳細データの種類変更時の確認画面
	

	// 標準入力データ
	delete inputData;
	// ユニット詳細データの追加画面
	AddUnitData::Release();
	if (addUnitData) delete addUnitData;
	// ユニット詳細データ
	UnitData::Release();
	for (int i = 0, n = (int)putUnit.size(); i < n; ++i) {
		for (int j = 0, m = (int)putUnit[i].unitData.size(); j < m; ++j) {
			delete putUnit[i].unitData[j];
		}
		putUnit[i].unitData.clear();
		putUnit[i].unitData.shrink_to_fit();
	}
	// エンカウントリスト
	encList.clear();
	encList.shrink_to_fit();
	// 削除したマップチップデータ（Ctrl+Zのやつ）
	preChipData.clear();
	preChipData.shrink_to_fit();
	// 削除したマップチップを戻す用の配置データ（Ctrl+Yのやつ）
	postChipData.clear();
	postChipData.shrink_to_fit();
	// マップチップのリスト（床）
	gr_mcGrIDList_FLO.clear();
	gr_mcGrIDList_FLO.shrink_to_fit();
	// マップチップのリスト（壁）
	gr_mcGrIDList_WAL.clear();
	gr_mcGrIDList_WAL.shrink_to_fit();
	// マップデータ
	for (int i = 0, n = (int)mapData.size(); i < n; ++i) {
		mapData[i].clear();
		mapData[i].shrink_to_fit();
	}
	mapData.clear();
	mapData.shrink_to_fit();
	// ユニットの画像のIDリスト
	unitGrIDList.clear();
	unitGrIDList.shrink_to_fit();
	// 削除したユニット配置データ（Ctrl+Zのやつ）
	prePutUnit.clear();
	prePutUnit.shrink_to_fit();

	// 一応画像全削除
	InitGraph();

	// パネルクラスの終了処理
	Panel::Fina();
}

void MapMaker::LoadMapChip() {
	// 【マップリスト】
	this->select_ChipList = eChipType::FLO;
	
	int Count_FLO = 0;	// 床の読み込み数
	int Count_WAL = 0;	// 壁

	// 読み込むファイルの指定
	std::ifstream lFile("UsingMapChip.csv");		// ファイルオープン

	if (lFile.fail()) {	// ファイル読み込み
		// 読み込み失敗(´・ω・)
		this->endFlag = true;
	}
	else {
		int id = 0;	// id
		int gr = 0;	// 画像データ

		std::string line;
		std::string token;

		// 読み込み成功
		while (getline(lFile, line)) {	// 一行ずつ読み込み
			std::istringstream stream(line);	// 読み込んだ行をコピー
			
			// 1【マップチップid】
			getline(stream, token, ',');
			id = boost::lexical_cast<int>(token);
			// 1【画像データ】
			getline(stream, token, ',');
			gr = LoadGraph(("img\\mapchip\\" + token).c_str());
			// マップチップのmapに追加
			gr_mapChip[id] = gr;
			if (id % 2 == 0) {
				// 【床】
				gr_mcGrIDList_FLO.push_back(id);
			}
			else {
				// 【壁】
				gr_mcGrIDList_WAL.push_back(id);
			}
		}
	}
	// ファイルを閉じる
	lFile.close();
}

void MapMaker::LoadUnitGr() {

	// 読み込むファイルの指定
	std::ifstream lFile("UnitNum.csv");		// ファイルオープン

	if (lFile.fail()) {	// ファイル読み込み
		// 読み込み失敗(´・ω・)
		this->endFlag = true;
	}
	else {
		int num = 0;

		std::string line;
		std::string token;

		// 読み込み成功
		while (getline(lFile, line)) {	// 一行ずつ読み込み
			std::istringstream stream(line);	// 読み込んだ行をコピー
			// ユニット画像データ読み込み
			getline(stream, token, ',');
			num = boost::lexical_cast<int>(token);
			getline(stream, token, ',');
			LoadDivGraph(("img\\unit\\" + token).c_str(), 16, 4, 4, 64, 64, gr_unit[num]);
			
		}
	}
	// ファイルを閉じる
	lFile.close();

	// グラフィックIDのリスト
	int count = 0;
	for (auto i = gr_unit.begin(), n = gr_unit.end(); i != n; ++i) {
		unitGrIDList.push_back(i->first);
	}
}

void MapMaker::UpDate() {

	//【標準入力】
	if (inputData) {
		if (inputData->UpDate()) {
			// 入力終了
			delete inputData;
			inputData = nullptr;
		}
		else {
			// 入力中
			return;
		}
	}

	// モード切替
	if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
		if (MouseData::GetY() > 48 && MouseData::GetY() <= 96) {
			if (MouseData::GetX() >= 0 && MouseData::GetX() < 200) {
				this->mode = eMakeType::Map;
				return;
			}
			else if (MouseData::GetX() >= 200 && MouseData::GetX() < 400) {
				this->mode = eMakeType::PutUnit;
				return;
			}
		}
	}
	
	// コマンド
	if (KeyData::Get(KEY_INPUT_LCONTROL) > 0) {
		//【マップ編集終了】
		if (KeyData::Get(KEY_INPUT_BACK) == 1) {
			this->nextScene = eScene::S_DataSelect;
			this->endFlag = true;
		}
	}

	// 選択中のマップ座標【マウスのあるチップの座標】を取得
	if (MouseData::GetX() + scrollX > 0 &&
		MouseData::GetX() + scrollX < mapData[0].size() * chipSize &&
		MouseData::GetY() + scrollY > 0 &&
		MouseData::GetY() + scrollY < mapData.size() * chipSize) {

		selectX = (int)((MouseData::GetX() + scrollX) / chipSize);
		selectY = (int)((MouseData::GetY() + scrollY) / chipSize);
	}
	else {
		selectX = -1;
		selectY = -1;
	}

	switch (this->mode){
	case eMakeType::Map:
		this->UpDate_Map();
		break;
	case eMakeType::PutUnit:
		this->UpDate_Unit();
		break;
	default:
		break;
	}
}

void MapMaker::UpDate_Map() {
	
	//【スクロール】
	{
		if (KeyData::Get(KEY_INPUT_A) >= 1) {	// 左
			this->scrollX -= 25;
		}
		if (KeyData::Get(KEY_INPUT_D) >= 1) {	// 右
			this->scrollX += 25;
		}
		if (KeyData::Get(KEY_INPUT_W) >= 1) {	// 上
			this->scrollY -= 25;
		}
		if (KeyData::Get(KEY_INPUT_S) >= 1) {	// 下
			this->scrollY += 25;
		}
		if (KeyData::Get(KEY_INPUT_LCONTROL) > 0) {
			if (MouseData::GetX() < 320 && MouseData::GetY() > 96) {
				// マップチップリストのスクロール
				switch (select_ChipList) {
				case eChipType::FLO:
					if (gr_mcGrIDList_FLO.size() > 70) {
						scroll_list_FLO += MouseData::GetWheel() * 50;
						if (scroll_list_FLO > 0) {
							scroll_list_FLO = 0;
						}
						if ((gr_mcGrIDList_FLO.size() / 5 + 1) * 64 + scroll_list_FLO < 920) {
							scroll_list_FLO = -((int)gr_mcGrIDList_FLO.size() / 5 + 1) * 64 + 920;
						}
					}
					break;
				case eChipType::WAL:
					if (gr_mcGrIDList_WAL.size() > 70) {
						scroll_list_WAL += MouseData::GetWheel() * 50;
						if (scroll_list_WAL > 0) {
							scroll_list_WAL = 0;
						}
						if ((gr_mcGrIDList_WAL.size() / 5 + 1) * 64 + scroll_list_WAL < 920) {
							scroll_list_WAL = -((int)gr_mcGrIDList_WAL.size() / 5 + 1) * 64 + 920;
						}
					}
					break;
				default:
					break;
				}
			}
			else {
				//【拡大縮小】
				chipSize += MouseData::GetWheel();
				if (chipSize <= 0) {
					chipSize = 1;	// サイズが0以下にならないように
				}
			}
		}
		else {
			// マップのスクロール
			if (KeyData::Get(KEY_INPUT_LSHIFT)) {
				scrollX -= MouseData::GetWheel() * 50;
			}
			else {
				scrollY -= MouseData::GetWheel() * 50;
			}
		}

	}
	// CTRLを押しているか判定
	if (KeyData::Get(KEY_INPUT_LCONTROL) > 0) {
		// 左クリック
		if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
			// エイリアス
			const short mouseX = MouseData::GetX();	// マウスのX座標
			const short mouseY = MouseData::GetY();	// マウスのY座標

			//【マップチップリスト枠内か判定】
			if (mouseX < 320 && mouseY > 96) {
				//【チップ種類切り替え】
				if (mouseY <= 160) {
					//【床】
					if (mouseX <= 64) {
						select_ChipList = eChipType::FLO;
					}
					//【壁】
					else if (mouseX <= 128) {
						select_ChipList = eChipType::WAL;
					}					
				}
				//【チップ選択】
				else {
					switch (select_ChipList){
					case eChipType::FLO:
					{
						// (Y座標 - 上端(160))
						// 選択したマスを計算
						const int selectN = ((MouseData::GetY() - 160 - scroll_list_FLO) / 64) * 5 + MouseData::GetX() / 64;
						// アウトオブレンジ対策
						if (selectN >= 0 && selectN < gr_mcGrIDList_FLO.size()) {
							selectChip.data = gr_mcGrIDList_FLO[selectN];
							selectChip.type = eChipType::FLO;	// チップの種類
							selectChip.x = selectN % 5;
							selectChip.y = selectN / 5;
						}
						break;
					}
					case eChipType::WAL:
					{
						const int selectN = ((MouseData::GetY() - 160 - scroll_list_WAL) / 64) * 5 + MouseData::GetX() / 64;
						// アウトオブレンジ対策
						if (selectN >= 0 && selectN < gr_mcGrIDList_WAL.size()) {
							selectChip.data = gr_mcGrIDList_WAL[selectN];
							selectChip.type = eChipType::WAL;	// チップの種類
							selectChip.x = selectN % 5;
							selectChip.y = selectN / 5;
						}
						break;
					}						
					default:
						break;
					}
				}
			}
		}

		// マップの行列を追加,削除
		if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {	// 左クリック
			// 上
			if (MouseData::GetX() >= 833 && MouseData::GetX() <= 929 && MouseData::GetY() >= 4 && MouseData::GetY() <= 100) {
				// +
				mapData.push_front(mapData[0]);
			}
			else if (MouseData::GetX() >= 948 && MouseData::GetX() <= 1044 && MouseData::GetY() >= 4 && MouseData::GetY() <= 100 && mapData.size() > 1) {
				// -
				mapData.pop_front();
			}
			// 左
			else if (MouseData::GetX() >= 500 && MouseData::GetX() <= 596 && MouseData::GetY() >= 420 && MouseData::GetY() <= 516) {
				// +
				for (int i = 0, n = (int)mapData.size(); i < n; i++) {
					mapData[i].push_front(-1);
				}
			}
			else if (MouseData::GetX() >= 500 && MouseData::GetX() <= 596 && MouseData::GetY() >= 527 && MouseData::GetY() <= 623 && mapData[0].size() > 1) {
				// -
				for (int i = 0, n = (int)mapData.size(); i < n; i++) {
					mapData[i].pop_front();
				}
			}
			// 下
			else if (MouseData::GetX() >= 833 && MouseData::GetX() <= 929 && MouseData::GetY() >= 980 && MouseData::GetY() <= 1076) {
				// +
				mapData.push_back(mapData.back());
			}
			else if (MouseData::GetX() >= 948 && MouseData::GetX() <= 1044 && MouseData::GetY() >= 980 && MouseData::GetY() <= 1076 && mapData.size() > 1) {
				// -
				mapData.pop_back();

			}
			// 右
			else if (MouseData::GetX() >= 1820 && MouseData::GetX() <= 1916 && MouseData::GetY() >= 420 && MouseData::GetY() <= 516) {
				// +
				for (int i = 0, n = (int)mapData.size(); i < n; i++) {
					mapData[i].push_back(-1);
				}
			}
			else if (MouseData::GetX() >= 1820 && MouseData::GetX() <= 1916 && MouseData::GetY() >= 527 && MouseData::GetY() <= 623 && mapData[0].size() > 1) {
				// -
				for (int i = 0, n = (int)mapData.size(); i < n; i++) {
					mapData[i].pop_back();
				}
			}
		}

		// 【マップデータのセーブ】（CTRL + Enter）
		if (KeyData::Get(KEY_INPUT_RETURN) == 1) {
			SaveMapData();
		}

		// 【もとに戻す】（CTRL + Z）
		if (!preChipData.empty()) {
			if (KeyData::Get(KEY_INPUT_Z) == 1 || KeyData::Get(KEY_INPUT_Z) >= 30) {
				// もとのデータを保存
				tSelectChip data;
				data.x = preChipData.back().x;
				data.y = preChipData.back().y;
				data.data = mapData[preChipData.back().y][preChipData.back().x];
				postChipData.push_back(data);
				// 戻す
				mapData[preChipData.back().y][preChipData.back().x] = preChipData.back().data;
				preChipData.pop_back();
			}
		}
		//【やり直し】（Ctrl + Y）
		if (!postChipData.empty()) {
			if (KeyData::Get(KEY_INPUT_Y) == 1 || KeyData::Get(KEY_INPUT_Y) >= 30) {
				// もとのデータを保存
				tSelectChip data;
				data.x = postChipData.back().x;
				data.y = postChipData.back().y;
				data.data = mapData[postChipData.back().y][postChipData.back().x];
				preChipData.push_back(data);
				// やり直し
				mapData[postChipData.back().y][postChipData.back().x] = postChipData.back().data;
				postChipData.pop_back();
			}
		}
	}
	else {
		// 選択中のマップチップを置く
		if (MouseData::Get(MOUSE_INPUT_LEFT) >= 1) {	// 左クリック
			PutChip();
		}
		// 選択中のマップ座標のチップを消す
		//if (MouseData::Get(3) >= 1) {	// ホイールクリック
		//	DeleteChip();
		//}
		// スポイト
		if (MouseData::Get(MOUSE_INPUT_RIGHT) >= 1 && (selectX != -1 && selectY != -1)) {	// 右クリック
			SetSelectChip(mapData[selectY][selectX]);
		}
	}

	// 【delay処理】
	// セーブ
	if (SaveSuccess > 0) {
		SaveSuccess--;
	}
}

void MapMaker::UpDate_Unit() {

	// ユニット配置データのIDが変わったらユニットデータを読み込みなおす
	if (unitIdChangeFlag) {
		LoadUnitData(selectUnit);
		unitIdChangeFlag = false;
	}
	
	//【ユニット詳細データの削除時の確認画面】
	if (deleteUnitDataWindow.flag) {
		// 左クリック
		if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
			if (MouseData::GetX() > 700 && MouseData::GetX() < 1300 &&
				MouseData::GetY() > 300 && MouseData::GetY() < 600) {

				//【はい】
				if (MouseData::GetX() > 748 && MouseData::GetX() < 970 &&
					MouseData::GetY() > 456 && MouseData::GetY() < 552) {
					selectUnit->unitData.erase(selectUnit->unitData.begin() + deleteUnitDataWindow.unitDataNum);
					deleteUnitDataWindow.flag = false;
				}
				//【いいえ】
				else if (MouseData::GetX() > 1030 && MouseData::GetX() < 1252 &&
					MouseData::GetY() > 456 && MouseData::GetY() < 552) {
					deleteUnitDataWindow.flag = false;
				}

			}
			else {
				// 画面外クリックで終了
				deleteUnitDataWindow.flag = false;
			}
		}
		// エンターでも削除
		else if (KeyData::Get(KEY_INPUT_RETURN) >= 1) {
			selectUnit->unitData.erase(selectUnit->unitData.begin() + deleteUnitDataWindow.unitDataNum);
			deleteUnitDataWindow.flag = false;
		}
	}
	// ユニット詳細データの種類変更時の確認画面
	else if (changeUnitDataWindow.flag) {
		// 左クリック
		if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
			if (MouseData::GetX() > 700 && MouseData::GetX() < 1300 &&
				MouseData::GetY() > 300 && MouseData::GetY() < 600) {
				//【はい】
				if (MouseData::GetX() > 748 && MouseData::GetX() < 970 &&
					MouseData::GetY() > 456 && MouseData::GetY() < 552) {
					delete selectUnit->unitData[selectUnitDataNum];	// 元データ削除
					selectUnit->unitData[selectUnitDataNum] = Factory_UnitData((int)changeUnitDataWindow.unitDataType, changeUnitDataWindow.selectTypeList);
					changeUnitDataWindow.flag = false;
				}
				//【いいえ】
				else if (MouseData::GetX() > 1030 && MouseData::GetX() < 1252 &&
					MouseData::GetY() > 456 && MouseData::GetY() < 552) {
					changeUnitDataWindow.flag = false;
				}

			}
			else {
				// 画面外クリックで終了
				changeUnitDataWindow.flag = false;
			}
		}
	}
	//【ユニット詳細データの追加画面】
	else if (addUnitData) {
		if (addUnitData->UpDate()) {
			delete addUnitData;
			addUnitData = nullptr;
		}
	}
	//【ユニット詳細データ編集画面】
	else if(unitDataSelectFlag) {
		// 編集
		if (selectUnit->unitData[selectUnitDataNum]->Editing()) {
			unitDataSelectFlag = false;
		}
	}
	//【ユニットの画像選択画面】
	else if (unitGrSelectFlag) {
		//【画像選択画面】
		if (MouseData::Get(MOUSE_INPUT_LEFT) == 1 && selectUnit) {
			if (MouseData::GetX() > 1150 && MouseData::GetX() < 1470 &&
				MouseData::GetY() > 46 && MouseData::GetY() < 806) {
				
				if (MouseData::GetY() > 156) {
					// (Y座標 - 上端(156))
					// 選択したマスを計算
					const int selectN = ((MouseData::GetY() - 156) / 64) * 5 + (MouseData::GetX() - 1150) / 64;

					// アウトオブレンジ対策
					if (selectN >= 0 && selectN < unitGrIDList.size()) {
						selectUnit->gr = unitGrIDList[selectN];
					}
				}
			}
			else {
				// 画面外クリックで戻る
				unitGrSelectFlag = false;
			}
		}

		// エンターキーでも戻る
		if (KeyData::Get(KEY_INPUT_RETURN) > 0) {
			unitGrSelectFlag = false;
		}
	}
	else {
		// スクロール
		if (KeyData::Get(KEY_INPUT_A) >= 1) {	// 左
			this->scrollX -= 25;
		}
		if (KeyData::Get(KEY_INPUT_D) >= 1) {	// 右
			this->scrollX += 25;
		}
		if (KeyData::Get(KEY_INPUT_W) >= 1) {	// 上
			this->scrollY -= 25;
		}
		if (KeyData::Get(KEY_INPUT_S) >= 1) {	// 下
			this->scrollY += 25;
		}
		if (KeyData::Get(KEY_INPUT_LCONTROL) > 0) {

			//【拡大縮小】
			chipSize += MouseData::GetWheel();
			if (chipSize <= 0) {
				chipSize = 1;	// サイズが0以下にならないように		
			}

		}
		else {
			if (selectUnit &&
				MouseData::GetX() > 1500 && MouseData::GetX() < 1900 &&
				MouseData::GetY() > 258 && MouseData::GetY() < 1000) {
				// 【ユニット詳細データのスクロール】
				if (selectUnit->unitData.size() > 23) {
					scroll_UnitData += MouseData::GetWheel() * 40;
					if (scroll_UnitData > 0) {
						scroll_UnitData = 0;
					}
					if ((selectUnit->unitData.size() + 1) * 32 + scroll_UnitData < 742) {
						scroll_UnitData = -((int)selectUnit->unitData.size() + 1) * 32 + 742;
					}
				}
				else {
					scroll_UnitData = 0;
				}
			}
			else {
				// マップのスクロール
				if (KeyData::Get(KEY_INPUT_LSHIFT)) {
					scrollX -= MouseData::GetWheel() * 50;
				}
				else {
					scrollY -= MouseData::GetWheel() * 50;
				}
			}
		}

		//【ユニットのデータの編集】
		if (MouseData::GetX() > 1500 && MouseData::GetX() < 1900) {
			if (MouseData::Get(MOUSE_INPUT_LEFT) == 1 && selectUnit) {
				//【ユニット配置データの変更】
					//【画像】
				if (MouseData::GetX() > 1810 && MouseData::GetY() > 56 && MouseData::GetY() < 152) {
					unitGrSelectFlag = true;
				}
				//【ID】
				else if (MouseData::GetY() > 56 && MouseData::GetY() < 88) {
					// 標準入力
					inputData = new InputNum(&selectUnit->id, "IDを入力してください");
					unitIdChangeFlag = true;
				}
				//【名前】
				else if (MouseData::GetY() > 88 && MouseData::GetY() < 120) {
					// 標準入力
					inputData = new InputStr(&selectUnit->name, "名前を入力してください");
				}
				//【当たり判定】
				//else if (MouseData::GetY() > 152 && MouseData::GetY() < 184) {
				//	selectUnit->collisionFlag = !selectUnit->collisionFlag;
				//}
				//【出現フラグ】
				//else if (MouseData::GetY() > 184 && MouseData::GetY() < 216) {
				//	selectUnit->appearFlag = !selectUnit->appearFlag;
				//}
				//【ユニット詳細データの編集】
				if (MouseData::GetY() > 258 && MouseData::GetY() < 1000) {
					// 選択したデータ算出
					int selectN = ((MouseData::GetY() - 258 - scroll_UnitData) / 32);
					// アウトオブレンジ対策
					if (selectN >= 0 && selectN < selectUnit->unitData.size()) {
						// 編集
						if (MouseData::GetX() < 1836) {
							selectUnitDataNum = selectN;
							unitDataSelectFlag = true;
						}
						// 追加
						else if (MouseData::GetX() < 1868) {
							addUnitData = new AddUnitData(selectUnit->unitData, selectN + 1);
						}
						// 削除
						else {
							deleteUnitDataWindow.flag = true;
							deleteUnitDataWindow.unitDataNum = selectN;
							//selectUnit->unitData.erase(selectUnit->unitData.begin() + selectN);
						}
					}
				}
			}
		}
		//【ユニット選択】
		else if (MouseData::Get(MOUSE_INPUT_LEFT) == 1 && (selectX != -1 && selectY != -1)) {
			// ユニットを左クリックで選択
			for (int i = 0, n = (int)this->putUnit.size(); i < n; i++) {
				if (selectX == putUnit[i].x && selectY == putUnit[i].y) {
					selectUnit = &putUnit[i];
					unitMoveFlag = true;
					//LoadUnitData(selectUnit->id);	// ユニットデータのロード
					scroll_UnitData = 0;
					break;
				}
			}
		}

		//【ユニットの移動】
		if (unitMoveFlag && MouseData::Get(MOUSE_INPUT_LEFT) >= 1) {
			if (selectX != -1 && selectY != -1) {
				bool flag = true;
				for (int i = 0, n = (int)this->putUnit.size(); i < n; i++) {
					if (selectX == putUnit[i].x && selectY == putUnit[i].y && selectUnit->id != putUnit[i].id) {
						// 他のユニットに重ならないように
						flag = false;
					}
				}
				if (flag) {
					selectUnit->x = selectX;
					selectUnit->y = selectY;
				}
			}
		}
		else {
			unitMoveFlag = false;
		}

		//【ユニット追加】
		if (KeyData::Get(KEY_INPUT_I) == 1) {
			putUnit.push_back({
				(int)putUnit.size(),// id
				"新しいユニット",	// 名前
				(signed)selectX,	// 座標
				(signed)selectY,
				0,					// 画像
				true,				// 当たり判定
				true,				// 出現フラグ
				0,					// 初期モード
			});
			// 追加したユニットを選択状態に
			selectUnit = &putUnit.back();
			// 画像選択画面へ
			unitGrSelectFlag = true;
			LoadUnitData(selectUnit);
			// ユニットデータの読み込み
		}

		//【ユニット削除】
		if (KeyData::Get(KEY_INPUT_DELETE) == 1 && selectUnit) {
			for (int i = 0, n = (int)putUnit.size(); i < n; ++i) {
				// IDが同じものを削除
				if (putUnit[i].id == selectUnit->id) {
					prePutUnit.push_back(putUnit[i]);	// 削除するデータを保存
					putUnit.erase(putUnit.begin() + i);	// 削除
					break;
				}
			}
			selectUnit = nullptr;	// 非選択に
		}
	}

	//【コマンド】
	if (KeyData::Get(KEY_INPUT_LCONTROL) > 0) {
		//【セーブ】Ctrl + Enter
		if (KeyData::Get(KEY_INPUT_RETURN) == 1) {
			SavePutUnit();
		}
		// 削除取り消し
		if (KeyData::Get(KEY_INPUT_Z) == 1 && !prePutUnit.empty()) {
			putUnit.push_back(prePutUnit.back());
			prePutUnit.pop_back();
		}
	}

	// 【delay処理】
	// セーブ
	if (this->SaveSuccess > 0) {
		this->SaveSuccess--;
	}
}

void MapMaker::Draw() {
	DrawGraph(0, 0, gr_Back, false);	// 背景

	// マップ表示
	for (int i = 0, n = (int)mapData.size(); i < n; i++) {
		for (int j = 0, m = (int)mapData[i].size(); j < m; j++) {
			if (j * chipSize - scrollX > -chipSize && j * chipSize - scrollX < 1920 && i * chipSize - scrollY > -chipSize && i * chipSize - scrollY < 1080) {
				DrawExtendGraph(j * chipSize - scrollX, i * chipSize - scrollY, j * chipSize - scrollX + chipSize, i * chipSize - scrollY + chipSize, gr_mapChip[mapData[i][j]], true);
				if (KeyData::Get(KEY_INPUT_LCONTROL) > 0) {
					DrawFormatString(j * chipSize - scrollX, i * chipSize - scrollY, WHITE, "%d", mapData[i][j]);
				}
			}			
		}
	}

	switch (mode) {
	case eMakeType::Map:
		Draw_Map();
		break;
	case eMakeType::PutUnit:
		Draw_Unit();
		break;
	default:
		break;
	}

	// モード
	DrawGraph(0, 0, gr_mode[(int)mode], true);
}

void MapMaker::Draw_Map() {
	// Ctrlを押しているか判定
	if (KeyData::Get(KEY_INPUT_LCONTROL) > 0) {
		// チップリスト表示
		DrawBox(0, 96, 320, 1080, WHITE, true);	// 背景
		switch (select_ChipList) {
		case eChipType::FLO:	// 床
			for (int i = (scroll_list_FLO < 0 ? (-scroll_list_FLO / 64) * 5 : 0),
				n = (int)gr_mcGrIDList_FLO.size();
				i < n && 160 + (i / 5) * 64 + scroll_list_FLO < 1080; ++i) {
				DrawGraph((i % 5) * 64, 160 + (i / 5) * 64 + scroll_list_FLO, gr_mapChip[gr_mcGrIDList_FLO[i]], true);
			}
			// チップリストの選択カーソル描画
			if (selectChip.type == eChipType::FLO) {
				DrawGraph(selectChip.x * 64 - 10, selectChip.y * 64 + 150 + scroll_list_FLO, gr_SelectChip, true);
			}
			break;
		case eChipType::WAL:	// 壁
			for (int i = (scroll_list_WAL < 0 ? (-scroll_list_WAL / 64) * 5 : 0),
				n = (int)gr_mcGrIDList_WAL.size();
				i < n && 160 + (i / 5) * 64 + scroll_list_FLO < 1080; ++i) {
				DrawGraph((i % 5) * 64, 160 + (i / 5) * 64 + scroll_list_WAL, gr_mapChip[gr_mcGrIDList_WAL[i]], true);
			}
			// チップリストの選択カーソル描画
			if (selectChip.type == eChipType::WAL) {
				DrawGraph(selectChip.x * 64 - 10, selectChip.y * 64 + 150 + scroll_list_WAL, gr_SelectChip, true);
			}
			break;
		}
		DrawGraph(0, 96, gr_ChipList[(int)select_ChipList], true);	// 枠

		// 行追加/削除の画像
		DrawGraph(0, 0, gr_AddLine, true);
	}
	else {
		// 選択中のチップ
		DrawBox(6, 102, 70, 166, WHITE, true);
		DrawGraph(6, 102, gr_SelectChip, true);
		DrawGraph(16, 112, gr_mapChip[selectChip.data], true);

		// マップの選択カーソル
		if (selectX != -1 && selectY != -1) {
			DrawExtendGraph(selectX * chipSize - scrollX, selectY * chipSize - scrollY, selectX * chipSize - scrollX + chipSize, selectY * chipSize - scrollY + chipSize, gr_Cursor, true);
		}
	}

	// セーブしました
	if (SaveSuccess > 0) {
		DrawStringToHandle(700, 0, "セーブしました", RED, Font::Get(eFont::SELECT));
	}
	
	DrawFormatStringToHandle(1500, 0, BLUE, Font::Get(eFont::UNIT2), "マップサイズ:X%d / Y%d", mapData[0].size(), mapData.size());
	DrawFormatStringToHandle(1500, 50, BLUE, Font::Get(eFont::UNIT2), "選択中のマス:X%d / Y%d", selectX, selectY);
}

void MapMaker::Draw_Unit() {
	
	// ユニット描画
	for (int i = 0, n = (int)this->putUnit.size(); i < n; i++) {
		DrawExtendGraph(putUnit[i].x * chipSize - scrollX, putUnit[i].y * chipSize - scrollY, putUnit[i].x * chipSize - scrollX + chipSize, putUnit[i].y * chipSize - scrollY + chipSize, gr_unit[putUnit[i].gr][4], true);
		DrawFormatStringToHandle(putUnit[i].x * chipSize - scrollX, putUnit[i].y * chipSize - scrollY - 20, BLUE, Font::Get(eFont::ID), "%d", putUnit[i].id);
	}
	
	// ユニットの画像リスト
	if (unitGrSelectFlag) {
		// 枠
		DrawGraph(1140, 46, gr_UnitGrBack, true);
		// 画像
		for (int i = 0, n = (int)unitGrIDList.size(); i < n; ++i) {
			DrawGraph(1150 + (i % 5) * 64, 156 + (i / 5) * 64, gr_unit[unitGrIDList[i]][4], true);
		}
	}

	//【ユニット選択中】
	if (selectUnit) {
		// 枠線
		//DrawBox(1490, 46, 1910, 1010, BLACK, true);
		//////////////////////////////////////////
		//////////////【詳細データ】//////////////
		//////////////////////////////////////////
		DrawBox(1500, 258, 1900, 1000, SKY, true);
		for (int i = (scroll_UnitData < 0 ? (-scroll_UnitData / 32) : 0),
			n = (int)selectUnit->unitData.size();
			i < n && (i) * 32 + scroll_UnitData < 742; ++i) {
			//DrawFormatString(1516, 268 + i * 32, WHITE, "%s", unitData[i].c_str());
			selectUnit->unitData[i]->DrawSimple(258 + i * 32 + scroll_UnitData);
		}

		//////////////////////////////////////////
		//////////////【配置データ】//////////////
		//////////////////////////////////////////

		//DrawBox(1500, 50, 1900, 1000, GRAY, true);
		//DrawGraph(1500, 56, gr_PutUnitStatus, true);
		DrawGraph(1490, 46, gr_PutUnitStatus, true);
		// 選択カーソル
		DrawExtendGraph(selectUnit->x * chipSize - scrollX, selectUnit->y * chipSize - scrollY, selectUnit->x * chipSize - scrollX + chipSize, selectUnit->y * chipSize - scrollY + chipSize, gr_Cursor, true);

		// 配置データ
		DrawFormatString(1572, 64, WHITE, "%d", selectUnit->id);
		DrawFormatString(1572, 96, WHITE, "%s", selectUnit->name.c_str());
		DrawFormatString(1572, 128, WHITE, "%d / %d", selectUnit->x, selectUnit->y);
		// DrawFormatString(1628, 160, WHITE, "%s", (selectUnit->collisionFlag ? "あり" : "なし"));
		// DrawFormatString(1628, 192, WHITE, "%s", (selectUnit->appearFlag ? "出現" : "消滅"));
		// DrawFormatString(1628, 224, WHITE, "%d", selectUnit->mode);
		DrawGraph(1824, 68, gr_unit[selectUnit->gr][4], true);
	}

	//【ユニット詳細データ編集画面】
	if (unitDataSelectFlag) {
		selectUnit->unitData[selectUnitDataNum]->DrawData();
	}

	//【ユニット詳細データの追加画面】
	else if (addUnitData) {
		addUnitData->Draw();
	}
	
	//【標準入力処理】
	if (inputData) {
		inputData->Draw();	// 入力処理
	}

	//【ユニット詳細データの種類変更時の確認画面】
	if (changeUnitDataWindow.flag) {
		DrawGraph(700, 300, changeUnitDataWindow.gr_Back, true);
	}

	//【ユニット詳細データの削除時の確認画面】
	if (deleteUnitDataWindow.flag) {
		DrawGraph(700, 300, deleteUnitDataWindow.gr_Back, true);
	}
	

	// セーブしました
	if (this->SaveSuccess > 0) {
		DrawStringToHandle(700, 0, "セーブしました", RED, Font::Get(eFont::SELECT));
	}
	
	DrawFormatStringToHandle(1500, 0, BLUE, Font::Get(eFont::UNIT2), "ユニット数:%d", putUnit.size());

}

// マップチップを置く
void MapMaker::PutChip() {
	if (selectX != -1 && selectY != -1) {
		if (MouseData::GetY() <= 96 && MouseData::GetX() <= 600) {
			return;	// モード切替の場所は置けないようにする
		}
		if (this->selectChip.data != this->mapData[this->selectY][this->selectX]) {
			// もとのデータを保存
			tSelectChip data;
			data.x = this->selectX;
			data.y = this->selectY;
			data.data = this->mapData[this->selectY][this->selectX];
			this->preChipData.push_back(data);
			// セーブ数は100まで
			if (this->preChipData.size() > 500) {
				this->preChipData.pop_front();
			}

			// チップを置く
			this->mapData[this->selectY][this->selectX] = this->selectChip.data;

			// やり直し用のデータを削除
			postChipData.clear();
			postChipData.shrink_to_fit();
		}
	}
}

// スポイト
void MapMaker::SetSelectChip(int data) {
	// チップデータ格納
	this->selectChip.data = data;
}

void MapMaker::LoadMapData() {

	int mapHeight = 0;		// マップの縦幅
	int mapWidth = 0;		// マップの横幅

	// 読み込むファイルの指定
	std::ifstream lFile("mapdata\\" + this->dataPath + ".csv");		// ファイルオープン

	if (lFile.fail()) {	// ファイル読み込み
		// 読み込み失敗(´・ω・)
		this->endFlag = true;
	}
	else {
		int loadCount = 0;
		std::string line;
		std::string token;

		// 読み込み成功
		{
			getline(lFile, line);
			stringstream stream(line);	// strをストリームとして持つ
			string token;				// 入力された文字列(カンマ区切り)を保存
			// 1【マップ横幅】
			getline(stream, token, ',');
			mapWidth = boost::lexical_cast<int>(token);
			// 2【マップ縦幅】
			getline(stream, token, ',');
			mapHeight = boost::lexical_cast<int>(token);
		}
		//////////////////////////////////////
		////////【2行目-エンカウント】////////
		//////////////////////////////////////
		{
			getline(lFile, line);
			stringstream stream(line);	// strをストリームとして持つ
			string token;				// 入力された文字列(カンマ区切り)を保存
			// 1【エンカウントフラグ】
			getline(stream, token, ',');
			encFlag = token == "1";
			// 2【最小エンカウント歩数】
			getline(stream, token, ',');
			encMinCount = boost::lexical_cast<int>(token);
			// 3【最大エンカウント歩数】
			getline(stream, token, ',');
			encMaxCount = boost::lexical_cast<int>(token);
			// 4【エンカウントid】
			while (getline(stream, token, ',')) {
				if (token == "") break;
				encList.push_back(boost::lexical_cast<int>(token));
			}
		}
		mapData.resize(mapHeight);		// 縦幅メモリ確保
		while (getline(lFile, line)) {	// 一行ずつ読み込み
			mapData[loadCount].resize(mapWidth);		// 横幅メモリ確保
			std::istringstream stream(line);	// 読み込んだ行をコピー
			int j = 0;
			while (getline(stream, token,',')) {	// ,ごとに区切る
				mapData[loadCount][j] = boost::lexical_cast<int>(token);
				++j;
			}

			loadCount++;	// 次の行に
		}
	}
	// ファイルを閉じる
	lFile.close();
}

void MapMaker::SaveMapData() {
	
	// 書き込むファイルの指定
	std::ofstream wFile("mapdata\\"+ dataPath + ".csv");	// ファイルオープン
	//std::ofstream wFile("mapdata\\.0.csv");	// ファイルオープン

	if (wFile.fail()) {	// ファイル書き込み
		// 書き込み失敗(´・ω・)
		endFlag = true;
	}
	else {
		// 書き込み成功
		//【1行目】
		// 1【マップ横幅】
		wFile << mapData[0].size() << ",";
		// 2【マップ縦幅】
		wFile << mapData.size() << ",";
		wFile << std::endl;

		//【2行目】
		// 1【エンカウントフラグ】
		wFile << (encFlag ? "1" : "0") << ",";
		// 2【最小エンカウント歩数】
		wFile << encMinCount << ",";
		// 3【最大エンカウント歩数】
		wFile << encMaxCount << ",";
		// 4【エンカウントid】
		for (int i = 0, n = (int)encList.size(); i < n; ++i) {
			wFile << encList[i] << ",";
		}
		wFile << std::endl;

		//【3行目以降】マップデータ
		for (int i = 0, n = (int)mapData.size(); i < n; i++) {
			for (int j = 0, m = (int)mapData[i].size() - 1; j < m; j++) {
				wFile << mapData[i][j] << ",";
			}
			wFile << mapData[i].back() << std::endl;
		}
		SaveSuccess = 60;	// "セーブしました"と表示
	}
	// ファイルを閉じる
	wFile.close();
	
}

void MapMaker::LoadPutUnit() {
	// 読み込むファイルの指定
	std::ifstream lFile("unitdata\\" + dataPath +"\\PutUnit.csv");	// ファイルオープン
	//std::ifstream lFile("unitdata\\2\\PutUnit.csv");	// ファイルオープン

	if (lFile.fail()) {	// ファイル読み込み
		// 読み込み失敗(´・ω・)
		endFlag = true;
	}
	else {
		int loadCount = 0;
		std::string line;
		std::string token;

		// 読み込み成功
		while (getline(lFile, line)) {	// 一行ずつ読み込み
			std::istringstream stream(line);	// 読み込んだ行をコピー
			putUnit.resize(++loadCount);	// ユニット追加
			
			// 1【ユニットID】
			getline(stream, token, ',');
			putUnit.back().id = boost::lexical_cast<int>(token);
			// 2【名前】取得
			getline(stream, token, ',');
			putUnit.back().name = token;
			// 3【X座標】取得（マップ座標）
			getline(stream, token, ',');
			putUnit.back().x = boost::lexical_cast<int>(token);
			// 4【Y座標】取得（マップ座標）
			getline(stream, token, ',');
			putUnit.back().y = boost::lexical_cast<int>(token);
			// 5【画像】取得
			getline(stream, token, ',');
			putUnit.back().gr = boost::lexical_cast<int>(token);
			/*
			// 6【当たり判定】取得
			getline(stream, token, ',');
			putUnit.back().collisionFlag = token == "1";
			// 7【出現フラグ】取得
			getline(stream, token, ',');
			putUnit.back().appearFlag = token == "1";
			// 8【現在のモード数】取得
			getline(stream, token, ',');
			putUnit.back().mode = boost::lexical_cast<int>(token);
			*/

			//【ユニットデータのロード】
			LoadUnitData(&putUnit.back());
		}
	}
	// ファイルを閉じる
	lFile.close();
}

void MapMaker::SavePutUnit() {
	// 書き込むファイルの指定
	std::ofstream wFile("unitdata\\" + dataPath +"\\PutUnit.csv");	// ファイルオープン
	//std::ofstream wFile("unitdata\\2\\PutUnit.csv");	// ファイルオープン

	if (wFile.fail()) {	// ファイル書き込み
		// 書き込み失敗(´・ω・)
		this->endFlag = true;
	}
	else {
		// 書き込み成功
		for (int i = 0, n = (int)putUnit.size(); i < n; ++i) {
			// 1【ユニットID】
			wFile << putUnit[i].id << ",";
			// 2【名前】
			wFile << putUnit[i].name << ",";
			// 3【X座標】（マップ座標）
			wFile << putUnit[i].x << ",";
			// 4【Y座標】（マップ座標）
			wFile << putUnit[i].y << ",";
			// 5【画像】
			wFile << putUnit[i].gr;
			// 6【当たり判定】
			//wFile << (putUnit[i].collisionFlag ? 1 : 0) << ",";
			// 7【出現フラグ】
			//wFile << (putUnit[i].appearFlag ? 1 : 0) << ",";
			// 8【現在のモード数】
			//wFile << putUnit[i].mode << "," << std::endl;
			wFile << std::endl;

			//【ユニットデータの保存】
			SaveUnitData(&putUnit[i]);
		}
		SaveSuccess = 60;	// "セーブしました"と表示
	}
	// ファイルを閉じる
	wFile.close();
}

void MapMaker::LoadUnitData(tPutUnit* unit) {
	// ユニットデータ初期化
	for (int i = 0, n = (int)unit->unitData.size(); i < n; ++i) {
		delete unit->unitData[i];
	}
	unit->unitData.clear();
	unit->unitData.shrink_to_fit();

	// 読み込むファイルの指定
	std::ifstream lFile("unitdata\\" + dataPath + "\\data\\" + to_string(unit->id) + ".csv");	// ファイルオープン
	if (lFile.fail()) {	// ファイル読み込み
		// 読み込み失敗
		// ファイルを作成する
		std::ofstream newData("unitdata\\" + dataPath + "\\data\\" + to_string(unit->id) + ".csv");	// ファイルオープン
		// 初期データ
		newData << "0,0,0,0" << std::endl;	// モード（条件なし）
		newData << "1,0" << std::endl;	// 移動（移動なし）
		newData.close();

		unit->unitData.push_back(Factory_UnitData("0,0,0,0"));
		unit->unitData.push_back(Factory_UnitData("1,0"));
	}
	else {
		std::string line;
		std::string token;

		// 読み込み成功
		while (getline(lFile, line)) {	// 一行ずつ読み込み
			unit->unitData.push_back(Factory_UnitData(line));
		}
	}
	// ファイルを閉じる 
	lFile.close();
}

// ユニットデータのセーブ
void MapMaker::SaveUnitData(tPutUnit* unit) {
	// 書き込むファイルの指定
	std::ofstream wFile("unitdata\\" + dataPath + "\\data\\" + to_string(unit->id) + ".csv");	// ファイルオープン

	if (wFile.fail()) {	// ファイル書き込み
		// 書き込み失敗(´・ω・)
		this->endFlag = true;
	}
	else {
		// 書き込み成功
		for (int i = 0, n = (int)unit->unitData.size(); i < n; ++i) {
			wFile << unit->unitData[i]->GetCsv();
		}
	}
	// ファイルを閉じる
	wFile.close();

	// ユニットの初期モードのセーブ
	SaveUnitInitMode();
}

// ユニットの初期モードのセーブ
void MapMaker::SaveUnitInitMode() {
	// 書き込むファイルの指定
	std::ofstream wFile("unitdata\\" + dataPath + "\\InitMode.csv");	// ファイルオープン

	for (int i = 0, n = (int)putUnit.size(); i < n; ++i) {
		wFile << putUnit[i].id << "," << putUnit[i].mode << endl;
	}

	// ファイルを閉じる
	wFile.close();
}