#include "Panel.h"
#include "Input.h"
#include "MapMaker.h"

int Panel::gr_panel;				// パネル

Panel::Panel(int x, int y, int x2, int y2, std::string name) :
	x(x),
	y(y),
	x2(x2),
	y2(y2),
	name(name)
{
}
Panel::~Panel(){
}
void Panel::Init() {
	gr_panel = LoadGraph("img\\mapCreater\\panel.png");
}
void Panel::Fina() {
	DeleteGraph(gr_panel);
}
bool Panel::UpDate() {
	if(CheckHit() == eCheckHit::End) {
		return true;
	}
	else {
		return false;
	}
}
void Panel::Draw() {
	DrawPanel();
}
eCheckHit Panel::CheckHit() {
	if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
		if (MouseData::GetX() > x && MouseData::GetX() < x2 &&
			MouseData::GetY() > y && MouseData::GetY() < y2) {
			return eCheckHit::Hit;
		}
		else {
			return eCheckHit::End;
		}
	}
	return eCheckHit::Through;
}
void Panel::DrawPanel() {
	DrawExtendGraph(x, y, x2, y2, gr_panel, false);
	DrawStringFToHandle(x + 10, y + 8, name.c_str(), BLUE, Font::Get(eFont::UNIT2));
}
// ---------------------------------------------------------------------------------------------------------------------------------- //
// --------------------------------------------------------【flag用のボタン】-------------------------------------------------------- //
FlagButton::FlagButton(int x, int y, int x2, int y2, std::string name, bool* flag, std::string trueStr, std::string falseStr) :
	Panel(x, y, x2, y2, name),
	flag(flag),
	trueStr(trueStr),
	falseStr(falseStr)
{

}
FlagButton::~FlagButton() {

}
bool FlagButton::UpDate() {
	switch (CheckHit()) {
	case eCheckHit::Hit:
		(*flag) = !(*flag);		// フラグ反転
		return false;
	case eCheckHit::End:
		return true;
	default:
		return false;
	}
}
void FlagButton::Draw() {
	DrawPanel();
	int tmp_y = 0;
	if (y2 - y < 64) {
		tmp_y = y + 8;
	}
	else {
		tmp_y = ((y + y2) / 2) + 8;
	}
	DrawStringFToHandle(x + 10, tmp_y, (*flag ? trueStr.c_str() : falseStr.c_str()), BLACK, Font::Get(eFont::UNIT2));
}
// ---------------------------------------------------------------------------------------------------------------------------------- //
// -----------------------------------------------------【単純数値変換のボタン】----------------------------------------------------- //
SwitchButton::SwitchButton(int x, int y, int x2, int y2, std::string name, int* target, int num):
	Panel(x, y, x2, y2, name),
	target(target),
	num(num)
{

}
SwitchButton::~SwitchButton() {

}
bool SwitchButton::UpDate() {
	switch (CheckHit()) {
	case eCheckHit::Hit:
		(*target) = num;		// 規定の数値を代入
		return false;
	case eCheckHit::End:
		return true;
	default:
		return false;
	}
}
void SwitchButton::Draw() {
	DrawPanel();
}
// ---------------------------------------------------------------------------------------------------------------------------------- //
// -------------------------------------------------------【数値入力のボタン】------------------------------------------------------- //
Button_InputNum::Button_InputNum(int x, int y, int x2, int y2, std::string name, int* target, std::string description) :
	Panel(x, y, x2, y2, name),
	target(target),
	description(description)
{

}
Button_InputNum::~Button_InputNum() {

}
bool Button_InputNum::UpDate() {
	switch (CheckHit()) {
	case eCheckHit::Hit:
		MapMaker::inputData = new InputNum(target, description);
		return false;
	case eCheckHit::End:
		return true;
	default:
		return false;
	}
}
void Button_InputNum::Draw() {
	DrawPanel();
	int tmp_y = 0;
	if (y2 - y < 64) {
		tmp_y = y + 8;
	}
	else {
		tmp_y = ((y + y2) / 2) + 8;
	}
	DrawFormatStringFToHandle(x + 10, tmp_y, BLACK, Font::Get(eFont::UNIT2), "%d", *target);
}
// ---------------------------------------------------------------------------------------------------------------------------------- //
// ------------------------------------------------------【プルダウンメニュー】------------------------------------------------------ //
PulldownMenu_int::PulldownMenu_int(int x, int y, int x2, int y2, std::string name, int* target, std::vector<std::string> menu) :
	Panel(x, y, x2, y2, name),
	target(target),
	menu(menu)
{
	scroll = 0;
	downFlag = false;
}
PulldownMenu_int::~PulldownMenu_int() {
	menu.clear();
	menu.shrink_to_fit();
}
bool PulldownMenu_int::UpDate() {
	if (downFlag) {
		// スクロール
		scroll -= MouseData::GetWheel() * 50;
		if (MouseData::Get(MOUSE_INPUT_LEFT) == 1) {
			if (MouseData::GetX() >= x && MouseData::GetX() <= x + 300 && MouseData::GetY() >= y2 - scroll && MouseData::GetY() <= y2 + (menu.size() * 32) - scroll) {
				*target = (MouseData::GetY() + scroll - y2) / 32;
			}
			downFlag = false;
		}
		return false;
	}
	else {
		switch (CheckHit()) {
		case eCheckHit::Hit:
			downFlag = true;
			scroll = 0;
			return false;
		case eCheckHit::End:
			return true;
		default:
			return false;
		}
	}
}
void PulldownMenu_int::Draw() {
	DrawPanel();
	if ((*target) >= 0 && (*target) < menu.size()) {
		int tmp_y = 0;
		if (y2 - y < 64) {
			tmp_y = y + 8;
		}
		else {
			tmp_y = ((y + y2) / 2) + 8;
		}
		DrawStringFToHandle(x + 10, tmp_y, menu[*target].c_str(), BLACK, Font::Get(eFont::UNIT2));
	}
	if (downFlag){
		for (int i = 0, n = (int)menu.size(); i < n; ++i) {
			DrawBox(x, y2 + (i * 32) - scroll, x + 300, y2 + (i * 32) - scroll + 32, BLACK, true);
			DrawBox(x, y2 + (i * 32) - scroll, x + 300, y2 + (i * 32) - scroll + 32, WHITE, false);
			DrawString(x + 6, y2 + (i * 32) - scroll + 6, menu[i].c_str(), WHITE);
		}
	}
}
// ---------------------------------------------------------------------------------------------------------------------------------- //
// -------------------------------------------------------【文字入力のボタン】------------------------------------------------------- //
Button_InputStr::Button_InputStr(int x, int y, int x2, int y2, std::string name, std::string* target, std::string description) :
	Panel(x, y, x2, y2, name),
	target(target),
	description(description)
{

}
Button_InputStr::~Button_InputStr() {

}
bool Button_InputStr::UpDate() {
	switch (CheckHit()) {
	case eCheckHit::Hit:
		MapMaker::inputData = new InputStr(target, description);
		return false;
	case eCheckHit::End:
		return true;
	default:
		return false;
	}
}
void Button_InputStr::Draw() {
	DrawPanel();
	int tmp_y = 0;
	if (y2 - y < 64) {
		tmp_y = y + 8;
	}
	else {
		tmp_y = ((y + y2) / 2) + 8;
	}
	DrawStringFToHandle(x + 10, tmp_y, (*target).c_str(), BLACK, Font::Get(eFont::UNIT2));
}