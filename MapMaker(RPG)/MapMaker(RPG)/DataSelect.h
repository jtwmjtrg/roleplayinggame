#pragma once
#include "Scene.h"
#include <Windows.h>
#include <stdexcept>
#include <vector>
#include <string>
#include "Font.h"
#include "Input.h"
// 作成するデータの選択

class DataSelect : public SuperScene{
private:
	//【画像】
	int gr_Back;	// 背景


	int select;			// 選択番号
	int selectNum;		// 選択項目数
	int scroll;			// スクロール
	int scroll_Max;		// スクロール限界

	std::string scenePath;	// パス
	std::vector < std::string > mapList;	// マップのリスト

	// ディレクトリ内検索
	std::vector < std::string > GetFolderName(std::string dir_Name);

public:
	DataSelect();
	~DataSelect();
	
	void UpDate();
	void Draw();

	std::string GetDataPath();	// パス取得
};