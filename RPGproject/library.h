#pragma once
#include <string>
#include <fstream>
#include <sstream>
#include <boost/lexical_cast.hpp>

namespace mylib {

	// トークン抽出（カンマ区切り等）
	// stringで返す
	std::string getToken_s(std::istringstream& stream, std::string& token, char punc = ',');
	// 返り値の指定
	template <typename T>
	T getToken(std::istringstream& stream, std::string& token, char punc = ','){
		getline(stream, token, punc);
		return boost::lexical_cast<T>(token);	// キャストして返す
	}
	/*// bool型を返す（"1"であればtrue）
	template <>
	bool getToken<bool>(std::istringstream& stream, std::string& token, char punc);*/

	// string を char* にキャスト
	char* StrToCharp(std::string str);

}