#include "library.h"

using namespace mylib;

std::string mylib::getToken_s(std::istringstream& stream, std::string& token, char punc) {
	getline(stream, token, punc);
	return token;	// そのまま返す
}
/*template <typename T>
T mylib::getToken(std::istringstream& stream, std::string& token, char punc) {
	getline(stream, token, punc);
	return boost::lexical_cast<T>(token);	// キャストして返す
}
/*
template <>
bool mylib::getToken<bool>(std::istringstream& stream, std::string& token, char punc) {
	getline(stream, token, punc);
	return token == "1";	// キャストして返す
}*/

// string を char* にキャスト
char* mylib::StrToCharp(std::string str) {
	char* cstr = new char[str.size() + 1];			// メモリ確保
	strcpy_s(cstr, str.size() + 1, str.c_str());	// コピー
	return cstr;
}